#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import sys
import os
import ast
from getopt import getopt, GetoptError
from cloudlock.services import run_service


try:
	opts, args = getopt(sys.argv[1:], 'dl:YEb:a:p:r:', ['daemon', 'logfile=', 'system-bus', 'session-bus', 'bus=', 'address=', 'path=', 'arguments=',])
	
	daemon = False
	logfile = None
	arguments = {}
	for opt, value in opts:
		if opt in ['-d', '--daemon']:
			daemon = True
		elif opt in ['-l', '--logfile']:
			logfile = value
		elif opt in ['-Y', '--system-bus']:
			arguments['bus'] = 'SYSTEM'
		elif opt in ['-E', '--session-bus']:
			arguments['bus'] = 'SESSION'
		elif opt in ['-b', '--bus']:
			arguments['bus'] = value
		elif opt in ['-a', '--address']:
			arguments['address'] = value
		elif opt in ['-p', '--path']:
			arguments['path'] = value
		elif opt in ['-r', '--arguments']:
			for a in value.split(";"):
				k, v = a.split("=")
				if k in ['bus', 'address', 'path']:
					raise GetoptError("Keys `bus`, `address` and `path` not allowed in arguments.")
				arguments[k] = ast.literal_eval(v)
	
	if len(args) != 1:
		raise GetoptError("Error: Exactly one service name needed")
	service = args[0]
	if service not in ['paxos', 'locker', 'rsync', 'network']:
		raise GetoptError("Error: Unknown service `" + service + "`")

except GetoptError as error:
	print(error)
	print("Usage: %s [-d|--daemon] [-l <path>|--logfile=<path>] \\" % sys.argv[0])
	print("       [-Y|--system-bus|-E|--session-bus|-b <bus>|--bus=<bus>] \\")
	print("       [-a <dbus-address>|--address=<dbus-address>] [-p <dbus-path>|--path=<dbus-path>] \\")
	print("       [-r '<name=value;...>'|--arguments='<name=value;...>'] <service>")
	print("Provide a service name: `paxos`, `locker`, `rsync` or `network`.")
	print("Use `-d` or `--daemon` to run in background.")
	print("Use `-l <path>` or `--logfile=<path>` to redirect standard output and error to a file.")
	print("To use system bus: `-Y`, `--system-bus`, `-b SYSTEM` or `--bus=SYSTEM`.")
	print("To use session bus: `-E`, `--session-bus`, `-b SESSION` or `--bus=SESSION`.")
	print("To use custom bus: `-b <bus>` or `--bus=<bus>`.")
	print("Attach to non-default dbus address: `-a <address>` or `--address=<address>`.")
	print("Attach to non-root dbus path (`/` by default): `-p <path>` or `--path=<path>`.")
	print("Supply arguments to the service: `-r <args>` or `--arguments=<args>`.")
	print("Arguments are semicolon-separated `arg=value` pairs, i.e. `-r 'debug=True;name=\\\"Service Name\\\"'`.")
	print("Supported argument values are: None, True, False, signed integers, signed floats and strings surrounded by single or double quotes.")
	quit(1)


if logfile is not None:
	logfile = open(logfile, 'a')


if daemon:
	pid = os.fork()
	if pid > 0:
		quit()
	elif pid < 0:
		print("Could not spawn a child process.")
		quit(2)


if logfile is not None:
	sys.stdout.flush()
	os.dup2(logfile.fileno(), sys.stdout.fileno())
	sys.stderr.flush()
	os.dup2(logfile.fileno(), sys.stderr.fileno())


if service == 'paxos':
	from cloudlock.services.paxos import setup, cleanup
	run_service(setup, cleanup, **arguments)
elif service == 'locker':
	from cloudlock.services.locker import setup, cleanup
	run_service(setup, cleanup, **arguments)
elif service == 'rsync':
	from cloudlock.services.rsync import setup, cleanup
	run_service(setup, cleanup, **arguments)
elif service == 'network':
	from cloudlock.services.network import setup, cleanup
	run_service(setup, cleanup, **arguments)
else:
	raise RuntimeError("Impossible condition.")


if logfile is not None:
	logfile.close()


