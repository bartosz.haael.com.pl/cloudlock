#!/bin/bash

if [ "${2}" = "" ]; then
 if [ "${1}" = "--clean" ]; then
  rm *.crt *.srl
  exit 0
 fi
 echo "Script for generating SSL test keys"
 echo "Usage: ${0} <domain_name> <number_of_keys>"
 echo "       ${0} --clean"
 echo "This script will generate one key for the supplied domain"
 echo "and N keys for its subdomains with names X.domain_name where"
 echo "X=0...N-1 with N=number_of_keys. The keys for subdomains"
 echo "will be signed with the master domain key."
 echo "To avoid confusion, redirect the standard error to /dev/null."
 echo "The keys will be saved to files of the name equal to the"
 echo "domain name with the extension 'crt'. On standard output the"
 echo "domain names and key passwords will be printed."
 exit 1
fi

gen_pass() {
dd if=/dev/urandom of=/dev/stdout bs=8 count=1 | base64
}

root_certificate() {
openssl req -x509 -newkey rsa:2048 -nodes -keyout "${1}".key.pem -outform pem -subj "/C=../ST=./L=./O=./OU=./CN=${1}/emailAddress=support@${1}" -out "${1}".crt.pem -days 7
openssl rsa -inform pem -in "${1}".key.pem -des3 -passout pass:"${2}" -outform pem -out "${1}".enckey.pem
cat "${1}".enckey.pem "${1}".crt.pem >"${1}".crt
rm "${1}".key.pem "${1}".enckey.pem "${1}".crt.pem
echo '1000' >"${1}".srl
echo "${1} ${2}"
}

domain_certificate() {
openssl req -new -newkey rsa:2048 -nodes -keyout "${1}".key.pem -outform pem -subj "/C=../ST=./L=./O=./OU=./CN=${1}/emailAddress=support@${1}" -out "${1}".req.pem
openssl rsa -inform pem -in "${1}".key.pem -des3 -passout pass:"${2}" -outform pem -out "${1}".enckey.pem
openssl x509 -outform pem -req -in "${1}".req.pem -CA "${3}".crt -passin pass:"${4}" -CAserial "${3}".srl -out "${1}".crt.pem -days 7
cat "${1}".enckey.pem "${1}".crt.pem >"${1}".crt
rm "${1}".key.pem "${1}".enckey.pem "${1}".req.pem "${1}".crt.pem
echo "${1} ${2}"
}

n="${2}"
root_pass="`gen_pass`"
root_certificate "${1}" "${root_pass}"
for ((i=0; i<n; i++)); do
 domain_certificate "${i}.${1}" "`gen_pass`" "${1}" "${root_pass}"
done


