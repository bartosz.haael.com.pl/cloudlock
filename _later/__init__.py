#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import nested_scopes, with_statement, generators, division, unicode_literals


from gi.repository import GObject
import dbus
import dbus.mainloop.glib

#from .resource import Resource
#from .request import Request
#from .rsync import ADDRESS as ADDRESS_RSYNC, INTERFACE_RSYNC, INTERFACE_RSYNC_MANAGER
#from .locker import ADDRESS as ADDRESS_LOCKER, INTERFACE_LOCKER
#from .paxos import ADDRESS as ADDRESS_PAXOS, INTERFACE_PAXOS, INTERFACE_PAXOS_MANAGER


__all__ = ['Resource', 'Request', 'main', 'bus', 'init', 'run', 'quit', 'finish']


main = None
bus = None

def init(setup):
	global main, bus
	main = GObject.MainLoop()
	dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	
	paxos_manager = dbus.Interface(bus.get_object(ADDRESS_PAXOS, '/'), INTERFACE_PAXOS_MANAGER) 
	paxos_path = paxos_manager.acquire('#default')
	
	rsync_manager = dbus.Interface(bus.get_object(ADDRESS_RSYNC, '/'), INTERFACE_RSYNC_MANAGER) 
	rsync_path = rsync_manager.acquire('#default', ADDRESS_PAXOS, paxos_path, INTERFACE_PAXOS)
	
	Resource.dbus_init(bus, ADDRESS_RSYNC, rsync_path, INTERFACE_RSYNC, ADDRESS_LOCKER, '/', INTERFACE_LOCKER)
	
	def run_setup():
		try:
			setup()
		except:
			main.quit()
		return False
	gobject.idle_add(run_setup)

def run():
	main.run()

def quit():
	main.quit()

def finish():
	rsync_manager = dbus.Interface(bus.get_object(ADDRESS_RSYNC, '/'), INTERFACE_RSYNC_MANAGER) 
	rsync_manager.release('#default')
	bus.close()



