#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import itertools
import random


DEPTH = 10
DENSITY = 100

def rand_islice(iterator):
	return itertools.islice(itertools.filterfalse((lambda n: random.randrange(DENSITY) == 0), iterator), DEPTH)


class Domain:
	def __init__(self, length, contains, iterator):
		self.__length = length
		self.__contains = contains
		self.__iterator = iterator
	
	def __contains__(self, element):
		return self.__contains(element)
	
	def __len__(self):
		return self.__length
	
	def __iter__(self):
		return self.__iterator()


class Algebra:
	__slots__ = ['content']
	
	def __new__(cls, *args, **kwargs):
		content = {}
		for key, value in enumerate(args):
			content[key] = value
		for key, value in kwargs:
			content[key] = value
		
		obj = object.__new__(cls)
		object.__setattr__(obj, 'content', content)
		return obj
	
	def __getitem__(self, key):
		return self.content[key]
	
	def __setitem__(self, key, value):
		raise KeyError("nope")
	
	def __delitem__(self, key):
		raise KeyError("nope")
	
	def __setattr__(self, key, value):
		raise AttributeError("nope")
	
	def __delattr__(self, key):
		raise AttributeError("nope")
	
	#subset = None
	
	@classmethod
	def domain(Class):
		return Domain(None, Class.domain_has, Class.domain_iter)
	
	@classmethod
	def domain_has(Class, element):
		return True
	
	@classmethod
	def domain_iter(Class):
		yield Class()
		i = 0
		while True:
			yield Class(i)
			i += 1
	
	@staticmethod
	def test_model(Class):
		for element in rand_islice(Class.domain()):
			assert element in Class.domain()
	
	def __eq__(self, other):
		if self is other:
			return True
		try:
			return self.content == other.content
		except AttributeError:
			return NotImplemented
	
	def __ne__(self, other):
		return not (self == other)


class SetWithPoint(Algebra):
	@property
	@staticmethod
	def POINT():
		return SetWithPoint()


class Magma(Algebra):
	def __mul__(self, other):
		pass
	
	@staticmethod
	def test_model(Class):
		Algebra.test_model(Class)
		for a in rand_islice(Class.domain()):
			for b in rand_islice(Class.domain()):
				assert (a * b) in Class.domain()


class Semigroup(Magma):
	@classmethod
	def test_model(Class):
		Magma.test_model(Class)
		for a in rand_islice(Class.domain()):
			for b in rand_islice(Class.domain()):
				for c in rand_islice(Class.domain()):
					assert (a * b) * c == a * (b * c)


class Monoid(Semigroup):
	UNIT = None
	
	@classmethod
	def test_model(Class):
		Semigroup.test_model(Class)
		for a in rand_islice(Class.domain()):
			assert Class.UNIT * a == a
			assert a * Class.UNIT == a



class Group(Monoid):
	pass


class AbelianGroup(Group):
	pass



Algebra.test_model(Algebra)
Magma.test_model(Magma)

quit()


class Algebra:
	def __new__(cls, **kwargs):
		obj = super().__new__(cls)
		for key, value in kwargs.items():
			super().__setattr__(obj, key, value)
		return obj
	
	def __eq__(self, other):
		if self is other:
			return True
		elif self.__class__ == other.__class__:
			return self.__dict__ == other.__dict__
		elif issubclass(self.__class__, other.__class__):
			return Algebra.__new__(other.__class__, **self.__dict__) == other
		else:
			return NotImplemented
	
	def __ne__(self, other):
		return not (self == other)
	
	def __repr__(self):
		return ''.join([self.__class__.__name__, '(', ', '.join(['='.join([str(_k), repr(_v)]) for (_k, _v) in self.__dict__.items()]), ')'])
	
	def __bool__(self):
		return bool(self.__dict__)


class Group(Algebra):
	pass


class Ring(Algebra):
	pass


class TrivialRing(Ring):
	def __eq__(self, other):
		res = super().__eq__(other)
		if res is not False:
			return res
		
		return other == Ring()
	
	def __add__(self, other):
		if issubclass(other.__class__, self.__class__):
			return TrivialRing()
		else:
			return NotImplemented
	
	def __mul__(self, other):
		if issubclass(other.__class__, self.__class__):
			return TrivialRing()
		else:
			return NotImplemented

'''
class ZeroRing(Algebra):
	def __eq__(self, other):
'''


class Modulo2Ring(Ring):
	def __eq__(self, other):
		res = super().__eq__(other)
		if res is not False:
			return res
		
		return self.value % 2 == other.value % 2


def test0():
	zero = Algebra(value=0)
	oneA = Algebra(value=1)
	oneB = Algebra(value=1)
	oneC = Algebra(**oneA.__dict__)
	
	assert zero == zero
	
	for one in [oneA, oneB, oneC]:
		assert zero != one
		assert one != zero
	
	for one0 in [oneA, oneB, oneC]:
		for one1 in [oneA, oneB, oneC]:
			assert one0 == one1

def test1():
	null = TrivialRing()
	alg0 = Algebra()
	alg1 = Algebra(value=1)
	
	assert alg0 == null
	assert null == alg0
	
	assert alg1 != null, repr(alg1) + " != " + repr(null)
	assert null != alg1


def test2():
	zero0 = Modulo2Ring(value=0)
	one0 = Modulo2Ring(value=1)
	zero1 = Modulo2Ring(value=2)
	one1 = Modulo2Ring(value=3)
	
	assert zero0 == zero1


test0()
test1()
test2()





