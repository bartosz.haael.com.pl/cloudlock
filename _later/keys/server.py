#!/usr/bin/python3
#-*- coding:utf-8 -*-

import socket
import ssl


context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
context.verify_mode = ssl.CERT_OPTIONAL
context.set_ciphers('HIGH:!aNULL:!eNULL')
context.load_verify_locations(capath="/etc/ssl/certs")
context.load_verify_locations(cafile="haael.net.ca-bundle")
context.load_cert_chain(certfile="haael.net.crt", keyfile="haael.net.pem")

bindsocket = socket.socket()
bindsocket.bind(("127.0.0.1", 6666))
bindsocket.listen(4)


def deal_with_client(connstream):
	data = connstream.read(1024)
	print(data)
	connstream.write(b"test test test test\n")


while True:
	newsocket, fromaddr = bindsocket.accept()
	connstream = context.wrap_socket(newsocket, server_side=True)
	try:
		deal_with_client(connstream)
	finally:
		connstream.shutdown(socket.SHUT_RDWR)
		connstream.close()



