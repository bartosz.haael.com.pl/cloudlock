#!/usr/bin/python3
#-*- coding:utf-8 -*-

import socket
import ssl


context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
context.verify_mode = ssl.CERT_REQUIRED
context.set_ciphers('HIGH:!aNULL:!eNULL')
context.load_verify_locations(capath="/etc/ssl/certs")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ssl_sock = context.wrap_socket(s, server_side=False)
ssl_sock.connect(("127.0.0.1", 6666))

print(ssl_sock.getpeercert())
#ssl.match_hostname(ssl_sock.getpeercert(), "haael.net")

#ssl_sock.write(b"hello hello hello hello\n")
#print(ssl_sock.read(1024))

ssl_sock.close()


