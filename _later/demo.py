#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import nested_scopes, with_statement, generators, division, unicode_literals


import dbus
import dbus.service
import dbus.exceptions


#__all__ = ['Rsync', 'ADDRESS', 'INTERFACE']


ADDRESS = 'net.haael.demo'
INTERFACE = 'net.haael.Demo'
MANAGER_INTERFACE = 'net.haael.Manager'


class Manager(dbus.service.Object):
	def __init__(self, bus, path):
		dbus.service.Object.__init__(self, dbus.service.BusName(ADDRESS, bus=bus), path)
		self.__bus = bus
		self.__objs = {}
	
	@dbus.service.method(dbus_interface=MANAGER_INTERFACE, in_signature='s', out_signature='o')
	def acquire(self, tag):
		try:
			return self.__objs[tag]
		except KeyError:
			self.__objs[tag] = Demo(self.__bus, '/' + tag, tag)
			return self.__objs[tag]
	
	@dbus.service.method(dbus_interface=MANAGER_INTERFACE, in_signature='s', out_signature='b')
	def release(self, tag):
		try:
			del self.__objs[tag]
			return True
		except KeyError:
			return False


class Demo(dbus.service.Object):
	def __init__(self, bus, path, tag):
		dbus.service.Object.__init__(self, dbus.service.BusName(ADDRESS, bus=bus), path)
		self.__tag = tag
	
	@dbus.service.method(dbus_interface=INTERFACE, in_signature='s', out_signature='s')
	def identity(self, s):
		return self.__tag + ' : ' + s


if __name__ == '__main__':
	from gi.repository import GObject as gobject
	from dbus.mainloop.glib import DBusGMainLoop
	
	DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	manager = Manager(bus=bus, path='/')
	try:
		gobject.MainLoop().run()
	except KeyboardInterrupt:
		print()
	del manager
	bus.close()



