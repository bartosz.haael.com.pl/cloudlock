#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


from subprocess import *
from os import devnull, environ
from sys import path
from time import sleep


env = dict(environ)
if 'PYTHONPATH' in environ:
	pythonpath = ':'.join(path[0], environ['PYTHONPATH'])
else:
	pythonpath = path[0]
env['PYTHONPATH'] = pythonpath


DEVNULL = open(devnull, 'w')


domain = 'example.net'
subdomains = 8


network_daemon = {}
paxos_daemon = {}

def cleanup():
	print("Cleaning up...")
	for daemon in paxos_daemon.values():
		daemon.terminate()
	for daemon in network_daemon.values():
		daemon.terminate()
	DEVNULL.close()


print("Generating SSL keys and preparing 'network' services...")
ports = []
config_result = []
generate_keys = Popen(['sh', '-c', 'cd ./keys/testing; ./generate.sh --clean; ./generate.sh ' + domain + ' ' + str(subdomains)], stderr=DEVNULL, stdout=PIPE)
for m, line in enumerate(generate_keys.stdout):
	domain_name, password = line.decode('utf-8')[:-1].split(' ')
	print(domain_name, password)
	if m == 0:
		assert domain_name == domain
		root_password = password
		continue
	n = m - 1
	network_name = 'net.haael.network' + str(n)
	daemon = Popen(['./service.py', '--address=' + network_name, '--arguments=debug=True;profile="' + network_name + '.png"', 'network'], stdout=DEVNULL, env=env)
	network_daemon[network_name] = daemon
	sleep(2)
	config_daemon = Popen(['./tests/network-config.py', network_name, str(n), domain, 'keys/testing/' + str(n) + "." + domain + '.crt', password, 'keys/testing/' + domain + '.crt'], stdout=PIPE, env=env)
	sleep(1)
	try:
		ports.append(config_daemon.stdout.read()[:-1])
	except ValueError:
		pass
	config_result.append(config_daemon.wait())
generate_keys.wait()

if not all(result == 0 for result in config_result):
	print("Network config fail.")
	cleanup()
	quit(1)



sleep(1)
print("Network test...")
network_test = Popen(['./tests/network.py', domain] + list(network_daemon.keys()) + [':'] + ports, env=env)
network_result = network_test.wait()
if network_result != 0:
	print("Network test failed.")



print("Preparing 'paxos' services...")
config_result = []
for n, network_name in enumerate(network_daemon.keys()):
	paxos_name = 'net.haael.paxos' + str(n)
	daemon = Popen(['./service.py', '--address=' + paxos_name, '--arguments=debug=True;profile="' + paxos_name + '.png"', 'paxos'], stdout=DEVNULL, env=env)
	paxos_daemon[paxos_name] = daemon
	sleep(1)



sleep(1)
print("Paxos test...")
paxos_test = Popen(['./tests/paxos.py', domain] + list(paxos_daemon.keys()) + [':'] + list(network_daemon.keys()), env=env)
paxos_result = paxos_test.wait()
if paxos_result != 0:
	print("Paxos test failed.")
	cleanup()
	quit(1)



print("Success!!!")


cleanup()









