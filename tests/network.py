#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals

import sys
import socket
import traceback
#import os
#import fcntl

from gi.repository import GObject as gobject

import dbus
from dbus.mainloop.glib import DBusGMainLoop

from cloudlock.interfaces import *
from cloudlock.services import *



def test(network_addresses, domain_name, ports):
	networks = []
	domains = []
	hosts = set()
	
	print("Initial sanity check.")
	
	for network_address in network_addresses:
		network = DBusClientObject(bus, network_address, "/", INTERFACE_NETWORK)
		networks.append(network)
		domain = network.get_domain(domain_name)._interface(INTERFACE_DOMAIN)
		domains.append(domain)
		
		def gen_closure():
			this_domain = domain
			def invitation(address, hostname, tcpaddr, udpaddr, outgoing, password, certificate):
				hostname = str(hostname)
				#print("invitation", hostname, "->", str(this_domain.host_name()))
				hosts.add(hostname)
				peer = this_domain.add_peer(hostname, certificate)._interface(INTERFACE_PEER)
				peer.configure(tcpaddr, udpaddr, outgoing, password)
				global semaphore
				semaphore += 1
			return invitation
		invitation = gen_closure()
		
		def gen_closure():
			this_domain = domain
			def rejection(code, message, address, details):
				global semaphore
				if code == 15:
					semaphore += 1
				else:
					print("Rejection", code, message)
					print(details)
					raise RuntimeError("Network rejected.")
			return rejection
		rejection = gen_closure()
		
		domain.connect_to_signal('peer_invitation', invitation)
		network.connect_to_signal('rejection', rejection)
		
		yield 0
	
	
	assert len(domains) == len(networks)
	
	print("Network joining test.")
	
	for k, domain in enumerate(domains):
		for port in ports:
			domain.invite(('127.0.0.1', port))
		
		yield len(ports) * 2 - 2 * k
	
	
	assert len(hosts) == len(domains)
	
	for domain in domains:
		name = str(domain.host_name())
		peers = [str(_peer) for _peer in domain.list_peers()]
		assert name in hosts
		assert name not in peers
		assert len(peers) == len(hosts) - 1
		assert set(peers) < hosts
	
	yield 0
	
	
	print("Message sending test.")
	
	messages = {}
	for domain in domains:
		peers = [str(_p) for _p in domain.list_peers()]
		domain_host_name = str(domain.host_name())
		messages[domain_host_name] = {}
		for peer_name in peers:
			peer = domain.get_peer(peer_name)._interface(INTERFACE_PEER)
			messages[domain_host_name][peer_name] = ":".join([domain_host_name, peer_name])
			def gen_closure():
				this_domain_name = domain_host_name
				this_peer_name = peer_name
				def recv_message(resource, address, payload):
					#print("recv_message", resource, address, payload, this_peer_name, this_domain_name, messages[this_peer_name][this_domain_name])
					assert resource == '/', "Resource is: " + repr(resource)
					assert address[0] == '127.0.0.1', "Address is: " + repr(address)
					assert payload['p'] == messages[this_peer_name][this_domain_name]
					global semaphore
					semaphore += 1
				return recv_message
			recv_message = gen_closure()
			peer.connect_to_signal('recv_message', recv_message)
	
	yield 0
	
	
	for domain in domains:
		peers = [str(_p) for _p in domain.list_peers()]
		domain_host_name = str(domain.host_name())
		n = 0
		for peer_name in peers:
			n += 1
			peer = domain.get_peer(peer_name)._interface(INTERFACE_PEER)
			#print(messages[domain_host_name][peer_name])
			peer.send_message('/', {'p':messages[domain_host_name][peer_name]})
		yield n
	
	
	#...
	
	
	#for network in networks:
	#	network.close()
	
	#print("success")
	global success
	success = True
	return
	
	
	
	
	jeden_in_local, jeden_in_remote = socket.socketpair()
	jeden_in_local.setblocking(False)
	jeden_in_remote.setblocking(False)
	def incoming_connection(resource):
		assert resource == "/b"
		jeden.accept_connection(resource, jeden_in_remote.fileno())
		global semaphore
		semaphore += 1
	jeden.connect_to_signal('incoming_connection', incoming_connection)
	def handle_data(_sock, cond):
		if cond & gobject.IO_IN:
			data = jeden_in_local.recv(1024)
			if data:
				assert data == b"a000b111c222", "Data is " + repr(data)
				global semaphore
				semaphore += 1
		if cond & gobject.IO_HUP:
			jeden_in_local.close()
			return False
		else:
			return True
	gobject.io_add_watch(jeden_in_local, gobject.IO_IN | gobject.IO_HUP, handle_data)
	
	dwa_in_local, dwa_in_remote = socket.socketpair()
	dwa_in_local.setblocking(False)
	dwa_in_remote.setblocking(False)
	def incoming_connection(resource):
		assert resource == "/a"
		dwa.accept_connection(resource, dwa_in_remote.fileno())
		global semaphore
		semaphore += 1
	dwa.connect_to_signal('incoming_connection', incoming_connection)
	def handle_data(_sock, cond):
		if cond & gobject.IO_IN:
			data = dwa_in_local.recv(1024)
			if data:
				assert data == b"x000y111z222", "Data is " + repr(data)
				global semaphore
				semaphore += 1
		if cond & gobject.IO_HUP:
			dwa_in_local.close()
			return False
		else:
			return True
	gobject.io_add_watch(dwa_in_local, gobject.IO_IN | gobject.IO_HUP, handle_data)
	
	yield 0
	
	
	jeden_out_local, jeden_out_remote = socket.socketpair()
	jeden.create_connection("/a", jeden_out_remote.fileno())
	
	yield 1
	
	
	jeden_out_local.sendall(b"x000y111z222")
	
	yield 1
	
	
	dwa_out_local, dwa_out_remote = socket.socketpair()
	dwa.create_connection("/b", dwa_out_remote.fileno())
	dwa_out_local.sendall(b"a000b111c222")
	
	yield 2


	port_C = port_B + 1
	network_C = DBusClientObject(bus, network_address_C, "/", INTERFACE_NETWORK)
	#domain_C = network_C.create_domain("trzy", "haael.net", "keys/testing/trzy.crt", "")._interface(INTERFACE_DOMAIN)
	domain_C = network_C.create_domain_debug("trzy", "haael.net", True, "keys/testing/trzy.crt", "", "", "", "keys/testing/authority.crt")._interface(INTERFACE_DOMAIN)
	while True:
		try:
			domain_C.tcp_open(('127.0.0.1', port_C))
			domain_C.udp_open(('127.0.0.1', port_C))
		except dbus.exceptions.DBusException as e:
			if e._dbus_error_name == 'org.freedesktop.DBus.Python.socket.error':
				port_C += 1
			else:
				raise
		else:
			print("Selected port C:", port_C)
			break
	domain_C.add_outgoing('127.0.0.1')
	
	yield 0
	
	
	# *** joining network ***
	
	def invitation(address, hostname, tcpaddr, udpaddr, outgoing, password, certificate):
		hostname = str(hostname)
		assert hostname in ["jeden", "dwa"]
		peer = domain_C.add_peer(hostname, certificate)._interface(INTERFACE_PEER)
		peer.configure(tcpaddr, udpaddr, outgoing, password)
		global semaphore
		semaphore += 1 # SEMAPHORE: 1
	
	def rejection(address, code, message, details):
		print("Rejection", code, message)
		print(details)
		raise RuntimeError("Network C rejected.")
	
	domain_C.connect_to_signal('peer_invitation', invitation)
	network_C.connect_to_signal('rejection', rejection)
	
	domain_C.invite(('127.0.0.1', port_A))
	domain_C.invite(('127.0.0.1', port_B))
	
	yield 4
	
	
	jeden_in_local.close()
	jeden_out_local.close()
	dwa_in_local.close()
	dwa_out_local.close()
	
	yield 0
	
	
	network_A.close()
	network_B.close()
	
	success = True



if __name__ == '__main__':
	success = False
	semaphore = 0
	sem_limit = 0
	
	domain_name = sys.argv[1]
	sep = sys.argv.index(":")
	network_addresses = sys.argv[2:sep]
	ports = [int(_p) for _p in sys.argv[sep+1:]]
	
	gobject.threads_init()
	mainloop = gobject.MainLoop()
	DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	
	test_gen = test(network_addresses, domain_name, ports)
	
	step_limit = 50
	
	def step():
		global semaphore, sem_limit, step_limit
		if sem_limit == semaphore:
			try:
				semaphore = 0
				sem_limit = next(test_gen)
			except StopIteration:
				mainloop.quit()
			except Exception as e:
				print("Exception in network test.")
				print(sys.exc_info()[0])
				print(e)
				print(traceback.format_exc())
				print()
				mainloop.quit()
		elif sem_limit > semaphore:
			mainloop.quit()
			raise RuntimeError("Timeout while waiting for an event. sem_limit=" + str(sem_limit) + " semaphore=" + str(semaphore))
		elif sem_limit < semaphore:
			mainloop.quit()
			raise RuntimeError("Too many events. sem_limit=" + str(sem_limit) + " semaphore=" + str(semaphore))
		
		step_limit -= 1
		if step_limit <= 0:
			print("Maximum step limit reached.")
			mainloop.quit()
		
		return False
	
	gobject.timeout_add(5000, lambda: gobject.idle_add(step) or True)
	
	try:
		mainloop.run()
	except KeyboardInterrupt:
		print("")
	
	bus.close()
	
	if not success:
		print("Network: fail.")
		quit(1)
	else:
		print("Network: success.")



