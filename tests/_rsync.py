#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import sys
import os
from gi.repository import GObject

import dbus
from dbus.mainloop.glib import DBusGMainLoop

from cloudlock.interfaces import *
from cloudlock.services import *


if __name__ == '__main__':
	try:
		rsync_address = sys.argv[1]
	except IndexError:
		rsync_address = ADDRESS_RSYNC
	
	try:
		paxos_address = sys.argv[2]
	except IndexError:
		paxos_address = ADDRESS_PAXOS
	
	mainloop = GObject.MainLoop()
	DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	
	root = os.getcwd() + "/"
	test_host = 'test'
	test_tag = 'test'
	
	paxos_manager = DBusClientObject(bus, paxos_address, "/", INTERFACE_MANAGER)
	paxos = paxos_manager.acquire(test_tag, ())._interface(INTERFACE_PAXOS)
	
	rsync_manager = DBusClientObject(bus, rsync_address, "/", INTERFACE_MANAGER)
	rsync = rsync_manager.acquire(test_tag, (root, test_host, ADDRESS_PAXOS, paxos))._interface(INTERFACE_RSYNC)
	
	def test():
		try:
			rsync.update('aaa', 'yyy')
			print(paxos.fetch('yyy'))
			print(rsync.version('yyy'))
		finally:
			mainloop.quit()
	
	GObject.idle_add(test)
	
	try:
		mainloop.run()
	except KeyboardInterrupt:
		print()
	
	rsync_manager.release(test_tag)
	paxos_manager.release(test_tag)
	
	bus.close()


