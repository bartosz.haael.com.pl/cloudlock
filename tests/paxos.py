#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import sys
from time import time
import traceback
from gi.repository import GObject as gobject

import dbus
from dbus.mainloop.glib import DBusGMainLoop

from cloudlock.interfaces import *
from cloudlock.services import *


def test_single(domain_name, paxos_address):
	print("Single node paxos test.")
	paxos_manager = DBusClientObject(bus, paxos_address, "/", INTERFACE_MANAGER)
	
	paxos = paxos_manager.acquire(domain_name, ())._interface(INTERFACE_PAXOS)
	paxos.set_peer_name(domain_name)
	
	#def message_out(*x):
	#	print("message_out", x)
	#paxos.connect_to_signal('message_out', message_out)
	
	def update(*x):
		global semaphore
		#print("update", x)
		semaphore += 1
	paxos.connect_to_signal('update', update)
	
	def failure(*x):
		print("failure", x)
	paxos.connect_to_signal('failure', failure)
	
	def error(*x):
		print("error", x)
	paxos.connect_to_signal('error', error)
	
	paxos.send('aaa', {'a':'a'})
	paxos.send('bbb', {'b':'b'})
	paxos.send('ccc', {'c':'c'})
	
	yield 3
	
	assert paxos.fetch('aaa') == {'a':'a'}
	assert paxos.fetch('bbb') == {'b':'b'}
	assert paxos.fetch('ccc') == {'c':'c'}
	
	yield 0
	
	paxos_manager.release(domain_name)


def test_double(domain_name, paxos_address_A, paxos_address_B):
	yield 0

def test_triple(domain_name, paxos_address_A, paxos_address_B, paxos_address_C):
	yield 0


def test_multi(domain_name, paxos_addresses, network_addresses):
	print("Multi node paxos test.")
	connection = []
	paxi = []
	for paxos_address, network_address in zip(paxos_addresses, network_addresses):
		paxos_manager = DBusClientObject(bus, paxos_address, '/', INTERFACE_MANAGER)
		paxos = paxos_manager.acquire(domain_name, ())._interface(INTERFACE_PAXOS)
		
		def update(paxos_address):
			def update(key, value, version):
				global semaphore
				print("update", paxos_address, key, dict([_x for _x in value.items()]), version)
				semaphore += 1
			return update
		paxos.connect_to_signal('update', update(paxos_address))
		
		def failure(paxos_address):
			def failure(key, value, version):
				print("failure", paxos_address, key, dict([_x for _x in value.items()]), version)
				print(" previous value = ", paxos.fetch(key)) #dict([(str(_x), _y) for (_x, _y) in paxos.fetch(key).items()])
			return failure
		paxos.connect_to_signal('failure', failure(paxos_address))
		
		def error(*x):
			print("error", x)
		paxos.connect_to_signal('error', error)
		
		network = DBusClientObject(bus, network_address, '/', INTERFACE_NETWORK)
		domain = network.get_domain(domain_name)._interface(INTERFACE_DOMAIN)
		
		def rejection(code, message, address, details):
			print("rejection:", code, message, address, details)
			#raise RuntimeError()
		
		network.connect_to_signal('rejection', rejection)
		
		def callback(_paxos, _domain, _resource, event_name, args):
			print("callback:", event_name)
		
		connection.append(ConnectPaxosWithDomain(paxos, domain, '/paxos'))
		#paxos.connect_paxos_with_domain(network_address, domain, '/paxos')
		paxi.append(paxos)
	
	yield 0
	
	print("Test 1/4...")
	paxi[0].send('a', {'1':'1'})
	
	yield len(paxos_addresses)
	
	for paxos in paxi:
		assert paxos.fetch('a') == {'1':'1'}
	
	yield 0
	
	print("Test 2/4...")
	paxi[0].send('b', {'2':'1'})
	
	yield len(paxos_addresses)
	
	for paxos in paxi:
		assert paxos.fetch('b') == {'2':'1'}
		assert paxos.fetch('a') == {'1':'1'}
	
	yield 0
	
	print("Test 3/4...")
	paxi[0].send('a', {'1':'2'})
	
	yield len(paxos_addresses)
	
	for paxos in paxi:
		assert paxos.fetch('a') == {'1':'2'}
		assert paxos.fetch('b') == {'2':'1'}
	
	yield 0
	
	print("Test 4/4...")
	paxi[0].send('a', {'1':'3'})
	paxi[1 % len(paxos_addresses)].send('b', {'2':'2'})
	paxi[2 % len(paxos_addresses)].send('c', {'3':'1'})
	
	yield len(paxos_addresses) * 3
	
	for paxos in paxi:
		assert paxos.fetch('a') == {'1':'3'}
		assert paxos.fetch('b') == {'2':'2'}
		assert paxos.fetch('c') == {'3':'1'}
	
	yield 0
	
	print("Conflict test...")
	
	paxi[0].send('a', {'winner':'0'})
	paxi[1 % len(paxos_addresses)].send('a', {'winner':'1'})
	
	yield len(paxos_addresses)
	
	for paxos in paxi:
		print(paxos.fetch('a'))
		#assert paxos.fetch('a') == {'1':'3'}
		#assert paxos.fetch('b') == {'2':'2'}
		#assert paxos.fetch('c') == {'3':'1'}
	
	yield 0
	
	for c in connection:
		c.close()
	for paxos_address, network_address in zip(paxos_addresses, network_addresses):
		paxos_manager = DBusClientObject(bus, paxos_address, '/', INTERFACE_MANAGER)
		paxos_manager.release(domain_name)


def test(domain_name, paxos_addresses, network_addresses):
	if len(paxos_addresses) >= 1:
		for x in test_single('test_tag', paxos_addresses[0]):
			yield x
	if len(paxos_addresses) >= 2:
		for x in test_double(domain_name, paxos_addresses[0], paxos_addresses[1]):
			yield x
	if len(paxos_addresses) >= 3:
		for x in test_triple(domain_name, paxos_addresses[0], paxos_addresses[1], paxos_addresses[2]):
			yield x
	if len(paxos_addresses) == len(network_addresses):
		for x in test_multi(domain_name, paxos_addresses, network_addresses):
			yield x
	global success
	success = True
	print("Paxos: success.")


if __name__ == '__main__':
	success = False
	semaphore = 0
	sem_limit = 0

	domain_name = sys.argv[1]	
	marker = sys.argv.index(':')
	paxos_addresses = sys.argv[2:marker]
	network_addresses = sys.argv[marker+1:]
	print(domain_name)
	print(marker)
	print(paxos_addresses)
	print(network_addresses)
	assert len(paxos_addresses) == len(network_addresses)
	
	#try:
	#	paxos_address = sys.argv[1]
	#except IndexError:
	#	paxos_address = ADDRESS_PAXOS
	
	mainloop = gobject.MainLoop()
	DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	
	test_gen = test(domain_name, paxos_addresses, network_addresses)
	
	step_limit = 50
	
	def step():
		global semaphore, sem_limit, step_limit
		print("step", time(), "semaphore=" + str(semaphore), "sem_limit=" + str(sem_limit))
		if sem_limit == semaphore:
			try:
				semaphore = 0
				sem_limit = next(test_gen)
			except StopIteration:
				mainloop.quit()
				return False
			except Exception as e:
				print("Exception in paxos test.")
				print(sys.exc_info()[0])
				print(e)
				print(traceback.format_exc())
				print()
				mainloop.quit()
				return False
		elif sem_limit > semaphore:
			mainloop.quit()
			raise RuntimeError("Timeout while waiting for an event. sem_limit=" + str(sem_limit) + " semaphore=" + str(semaphore))
		elif sem_limit < semaphore:
			mainloop.quit()
			raise RuntimeError("Too many events. sem_limit=" + str(sem_limit) + " semaphore=" + str(semaphore))
		
		step_limit -= 1
		if step_limit <= 0:
			print("Maximum step limit reached.")
			mainloop.quit()
			return False
		
		#print("setting timeout:", sem_limit * 1000 + 1000)
		
		gobject.timeout_add(sem_limit * 1500 + 1000, lambda: gobject.idle_add(step) and False)
		#gobject.timeout_add(18000, lambda: gobject.idle_add(step) and False)
		return False
	
	try:
		step()
		mainloop.run()
	except KeyboardInterrupt:
		print()
	
	bus.close()


