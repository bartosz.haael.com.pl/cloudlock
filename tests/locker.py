#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import sys
from gi.repository import GObject

import dbus
from dbus.mainloop.glib import DBusGMainLoop

from cloudlock.interfaces import *
from cloudlock.services import *


def lock_when_unlocked(filename):
	try:
		locker.unlock_sh(filename)
		
		if locker.is_locked(filename) == (False, False):
			assert locker.lock_sh(filename) == True
			assert locker.is_locked(filename) == (True, False)
		else:
			raise RuntimeError("State preparation fail.")
	finally:
		locker.unlock_sh(filename)


def lock_when_locked(filename):
	try:
		locker.lock_sh(filename)
		
		if locker.is_locked(filename) == (True, False):
			assert locker.lock_sh(filename) == True
			assert locker.is_locked(filename) == (True, False)
		else:
			raise RuntimeError("State preparation fail.")
	finally:
		locker.unlock_sh(filename)
		locker.unlock_sh(filename)


def unlock_when_unlocked(filename):
	try:
		locker.unlock_sh(filename)
		
		if locker.is_locked(filename) == (False, False):
			assert locker.unlock_sh(filename) == False
			assert locker.is_locked(filename) == (False, False)
		else:
			raise RuntimeError("State preparation fail.")
	finally:
		pass


def unlock_when_locked(filename):
	try:
		locker.lock_sh(filename)
		
		if locker.is_locked(filename) == (True, False):
			assert locker.unlock_sh(filename) == True
			assert locker.is_locked(filename) == (False, False)
		else:
			raise RuntimeError("State preparation fail.")
	finally:
		locker.unlock_sh(filename)


def lock_twice(filename):
	try:
		locker.unlock_sh(filename)
		
		if locker.is_locked(filename) == (False, False):
			assert locker.lock_sh(filename) == True
			assert locker.is_locked(filename) == (True, False)
			assert locker.lock_sh(filename) == True
			assert locker.is_locked(filename) == (True, False)
			assert locker.unlock_sh(filename) == True
			assert locker.is_locked(filename) == (True, False)
			assert locker.unlock_sh(filename) == True
			assert locker.is_locked(filename) == (False, False)
		else:
			raise RuntimeError("State preparation fail.")
	finally:
		locker.unlock_sh(filename)
		locker.unlock_sh(filename)


def unlock_twice(filename):
	try:
		locker.unlock_sh(filename)
		
		if locker.is_locked(filename) == (False, False):
			assert locker.lock_sh(filename) == True
			assert locker.is_locked(filename) == (True, False)
			assert locker.unlock_sh(filename) == True
			assert locker.is_locked(filename) == (False, False)
			assert locker.unlock_sh(filename) == False
			assert locker.is_locked(filename) == (False, False)
		else:
			raise RuntimeError("State preparation fail.")
	finally:
		locker.unlock_sh(filename)


if __name__ == '__main__':
	try:
		locker_address = sys.argv[1]
	except IndexError:
		locker_address = ADDRESS_LOCKER
	
	mainloop = GObject.MainLoop()
	DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	
	locker = DBusClientObject(bus, locker_address, "/", INTERFACE_LOCKER)
	
	def test():
		try:
			lock_when_unlocked("jeden.txt")
			lock_when_locked("dwa.txt")
			unlock_when_unlocked("trzy.txt")
			unlock_when_locked("cztery.txt")
			lock_twice("piec.txt")
			unlock_twice("szesc.txt")
		finally:
			mainloop.quit()
	
	GObject.idle_add(test)
	
	try:
		mainloop.run()
	except KeyboardInterrupt:
		print()
	
	bus.close()


