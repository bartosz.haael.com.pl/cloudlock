#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import nested_scopes, with_statement, generators, division, unicode_literals


#import subprocess
import sys
import cloudlock

import dbus


'''
cloudlock.Resource.rsync = dbus.Interface(bus.get_object(RSYNC_ADDRESS, '/'), RSYNC_INTERFACE)
cloudlock.Resource.rsync.connect_to_signal('synchronized', lambda l, r: self.synchronized(l, r))
cloudlock.Resource.locker = dbus.Interface(bus.get_object(LOCKER_ADDRESS, '/'), LOCKER_INTERFACE)
cloudlock.Resource.locker.connect_to_signal('locked', lambda r: self.locked(r))
cloudlock.Resource.locker.connect_to_signal('unlocked', lambda r: self.unlocked(r))
'''


class CreateFile(cloudlock.Request):
	def __init__(self):
		cloudlock.Request.__init__(self)
		self.file0 = cloudlock.Resource("first.txt", "/first.txt", "wb")
	
	def prepare(self):
		print("prepare")
		self.ex(self.file0)
	
	def execute(self):
		print("execute")
		self.file0.seek(0)
		self.file0.write(b"Hello, void.\n")
	
	def finish(self):
		print("finish")
		cloudlock.Request.finish(self)
		cloudlock.quit()


class PrintFile(cloudlock.Request):
	def __init__(self):
		cloudlock.Request.__init__(self)
		self.file0 = cloudlock.Resource("first.txt", "/first.txt", "rb")
	
	def prepare(self):
		print("prepare")
		self.sh(self.file0)
	
	def execute(self):
		print("execute")
		self.file0.seek(0)
		print(self.file0.read())
	
	def finish(self):
		print("finish")
		cloudlock.Request.finish(self)
		cloudlock.quit()


class CopyFile(cloudlock.Request):
	def __init__(self):
		cloudlock.Request.__init__(self)
		self.file_in = cloudlock.Resource("first.txt", "/first.txt", "rb")
		self.file_out = cloudlock.Resource("second.txt", "/second.txt", "wb")
	
	def prepare(self):
		print("prepare")
		self.sh(self.file_in)
		self.ex(self.file_out)
	
	def execute(self):
		print("execute")
		self.file_in.seek(0)
		self.file_out.seek(0)
		data = self.file_in.read(4096)
		while data:
			self.file_out.write(data)
			data = self.file_in.read(4096)
	
	def finish(self):
		print("finish")
		cloudlock.Request.finish(self)
		cloudlock.quit()


#paxos = subprocess.Popen("cloudlock/paxos.py")
#rsync = subprocess.Popen("cloudlock/rsync.py")
#locker = subprocess.Popen("cloudlock/locker.py")

def setup():
	print("begin_setup")
	print("init")
	request = CreateFile()
	print("serve")
	request.serve()
	print("end_setup")

cloudlock.init(setup)
try:
	cloudlock.run()
except KeyboardInterrupt:
	print()
cloudlock.finish()


#locker.terminate()
#rsync.terminate()
#paxos.terminate()


