#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals

import sys
import socket
import traceback
#import os
#import fcntl

from gi.repository import GObject as gobject

import dbus
from dbus.mainloop.glib import DBusGMainLoop

from cloudlock.interfaces import *
from cloudlock.services import *




def configure(network_address, host, domain_name, certificate, password, authority):
	port = 4130
	network = DBusClientObject(bus, network_address, "/", INTERFACE_NETWORK)
	domain = network.create_domain_debug(host, domain_name, True, certificate, "", password, "", authority)._interface(INTERFACE_DOMAIN)
	while True:
		try:
			domain.tcp_open(('127.0.0.1', port))
			domain.udp_open(('127.0.0.1', port))
		except dbus.exceptions.DBusException as e:
			if port >= 65535:
				raise
			prefix = '.'.join(e._dbus_error_name.split('.')[:4])
			suffix = '.'.join(e._dbus_error_name.split('.')[4:])
			if prefix == 'org.freedesktop.DBus.Python' and suffix in ['socket.error', 'OSError']:
				port += 1
			else:
				raise
		else:
			print(port)
			break
	domain.add_outgoing('127.0.0.1')


if __name__ == '__main__':
	network_address = sys.argv[1]
	host = sys.argv[2]
	domain_name = sys.argv[3]
	certificate = sys.argv[4]
	password = sys.argv[5]
	authority = sys.argv[6]
	
	gobject.threads_init()
	mainloop = gobject.MainLoop()
	DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()

	success = False
	def work():
		global success
		try:
			configure(network_address, host, domain_name, certificate, password, authority)
			success = True
		finally:
			mainloop.quit()
	
	gobject.idle_add(work)
	
	try:
		mainloop.run()
	except KeyboardInterrupt:
		print("")
	
	bus.close()
	
	if not success:
		quit(1)
	


