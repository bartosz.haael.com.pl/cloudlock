#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import sys
from gi.repository import GObject

import dbus
from dbus.mainloop.glib import DBusGMainLoop

from cloudlock.interfaces import *
from cloudlock.services import *
from cloudlock.resource import *


if __name__ == '__main__':
	mainloop = GObject.MainLoop()
	DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	
	rsync_manager = dbus.Interface(bus.get_object(ADDRESS_RSYNC, '/'), INTERFACE_MANAGER)
	rsync_obj_name = 'test_resource'
	Resource.rsync = dbus.Interface(bus.get_object(ADDRESS_RSYNC, rsync_manager.acquire(rsync_obj_name)), INTERFACE_RSYNC)
	Resource.rsync.connect_to_signal('synchronized', lambda l, r: self.synchronized(l, r))
	Resource.locker = dbus.Interface(bus.get_object(ADDRESS_LOCKER, '/'), INTERFACE_LOCKER)
	Resource.locker.connect_to_signal('locked', lambda r: self.locked(r))
	Resource.locker.connect_to_signal('unlocked', lambda r: self.unlocked(r))
	
	def test():
		try:
			pass
		finally:
			mainloop.quit()
	
	GObject.idle_add(test)
	
	try:
		mainloop.run()
	except KeyboardInterrupt:
		print()
	
	rsync_manager.release(rsync_obj_name)
	
	bus.close()


