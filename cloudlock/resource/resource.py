#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import io
import fcntl
import weakref

#from .rsync  import ADDRESS as RSYNC_ADDRESS,  INTERFACE as RSYNC_INTERFACE
#from .locker import ADDRESS as LOCKER_ADDRESS, INTERFACE as LOCKER_INTERFACE


__all__ = ['Resource']


class Resource:
	__class__ = io.IOBase
	locker = None
	rsync = None
	__instances = []
	
	@classmethod
	def dbus_init(self, bus, rsync_address, rsync_path, rsync_interface, locker_address, locker_path, locker_interface):
		import dbus
		self.rsync = dbus.Interface(bus.get_object(rsync_address, rsync_path), rsync_interface)
		self.rsync.connect_to_signal('synchronized', lambda l, r: self.synchronized(l, r))
		self.locker = dbus.Interface(bus.get_object(locker_address, locker_path), locker_interface)
		self.locker.connect_to_signal('locked', lambda r: self.locked(r))
		self.locker.connect_to_signal('unlocked', lambda r: self.unlocked(r))
	
	@classmethod
	def synchronized(self, local, remote):
		for n, reference in enumerate(self.__instances):
			resource = reference()
			if resource is None:
				del self.__instances[n]
				continue
			if (resource.__local == local) and (resource.__remote == remote):
				if resource.__sh_locking:
					resource.__lock_sh_synchronized()
				if resource.__ex_locking:
					resource.__lock_ex_synchronized()
	
	@classmethod
	def locked(self, remote):
		for n, reference in enumerate(self.__instances):
			resource = reference()
			if resource is None:
				del self.__instances[n]
				continue
			if (resource.__remote == remote):
				if resource.__ex_lock_pending:
					resource.__lock_ex_acquired()
	
	@classmethod
	def unlocked(self, remote):
		for n, reference in enumerate(self.__instances):
			resource = reference()
			if resource is None:
				del self.__instances[n]
				continue
			if (resource.__remote == remote):
				if resource.__sh_locking:
					resource.lock_sh()
				if resource.__ex_locking:
					resource.lock_ex()
	
	def __init__(self, local, remote, *args, **kwargs):
		self.__io = open(local, *args, **kwargs)
		self.__instances.append(weakref.ref(self))
		self.__local = local
		self.__remote = remote
		self.__sh_locking = False
		self.__ex_locking = False
		self.__ex_lock_pending = False
		self.locked_sh = False
		self.locked_ex = False
		self.on_locked_sh = lambda s: None
		self.on_locked_ex = lambda s: None
		self.on_unlocked_sh = lambda s: None
		self.on_unlocked_ex = lambda s: None
	
	def lock_sh(self):
		self.__sh_locking = True
		try:
			fcntl.flock(self, fcntl.LOCK_SH | fcntl.LOCK_NB)
		except IOError:
			return False
		r = self.locker.lock_sh(self.__remote)
		if r == False:
			fcntl.flock(self, fcntl.LOCK_UN)
			return False
		r = self.rsync.synchronize(self.__local, self.__remote)
		if r == True:
			self.__lock_sh_synchronized()
		return True
	
	def __lock_sh_synchronized(self):
		self.__sh_locking = False
		self.locked_sh = True
		self.on_locked_sh(self)
	
	def unlock_sh(self):
		self.locker.unlock_sh(self.__remote)
		self.locked_sh = False
		fcntl.flock(self, fcntl.LOCK_UN)
		self.on_unlocked_sh(self)
	
	def lock_ex(self):
		self.__ex_locking = True
		try:
			fcntl.flock(self, fcntl.LOCK_SH | fcntl.LOCK_NB)
		except IOError:
			print("flock")
			return False
		r = self.locker.lock_ex(self.__remote)
		if r == False:
			print("locker")
			fcntl.flock(self, fcntl.LOCK_UN)
			return False
		self.__ex_lock_pending = True
		return True
	
	def __lock_ex_acquired(self):
		self.__ex_lock_pending = False
		try:
			fcntl.flock(self, fcntl.LOCK_EX | fcntl.LOCK_NB)
		except IOError:
			self.locker.unlock_ex(self.__remote)
			fcntl.flock(self, fcntl.LOCK_UN)
			raise
		r = self.rsync.synchronize(self.__local, self.__remote)
		if r == True:
			self.__lock_ex_synchronized()
	
	def __lock_ex_synchronized(self):
		self.__ex_locking = False
		self.locked_ex = True
		self.on_locked_ex(self)
	
	def unlock_ex(self):
		self.rsync.update(self.__local, self.__remote)
		self.locker.unlock_ex(self.__remote)
		self.locked_ex = False
		fcntl.flock(self, fcntl.LOCK_UN)
		self.on_unlocked_ex(self)
	
	def close(self):
		self.on_locked_sh = lambda s: None
		self.on_locked_ex = lambda s: None
		self.on_unlocked_sh = lambda s: None
		self.on_unlocked_ex = lambda s: None
		if self.locked_sh:
			self.unlock_sh()
		if self.locked_ex:
			self.unlock_ex()
		self.__io.close()
		for n, reference in enumerate(self.__instances):
			if reference() is self:
				del self.__instances[n]
				break
	
	def __getattr__(self, attr):
		return getattr(self.__io, attr)


