#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


__all__ = ['Request', 'NOTHING']


NOTHING = lambda s: None


class Request(object):
	"""
	A class that allows serving requests using shared resources.
	The operation is: declare resources, try to lock them, when all resources are locked then serve the request and release everything in the end.
	To use this class, you need to subclass it. In the constructor, declare all the `Resource` objects. In the `prepare` method, call `sh` and `ex`
	methods on the resources to acquire a shared lock or an exlusive lock, respectively. This method may be called several times until all locks
	are granted. If any of the resources can not be locked, all locks are released, the class begins waiting for the offending resources to be free
	and then the `prepare` method is retried. When this finally succeeds, the `execute` method is invoked. In this method, the actual request
	should be served, using the locked resources. After that, the resources get unlocked and then the `finish` method is called. The default
	implementation of this method is to call `close` on all resources. The resources are unlocked and the `finish` is called even if the `execute`
	method raised an exception.
	To start the request you need to call the `serve` method. You can reuse the `Request` object and call `serve` once again, provided that all the
	resources declared in the constructor are in the usable state. You may reset them in the `finish` method or re-initialize them. But in that case
	provide another method (i.e. `close`) that cleans up all resources and call it directly.
	"""
	
	__UNLOCK    = 'UNLOCK'
	__UNLOCK_P  = 'UNLOCK_PENDING'
	__LOCK_EX_S = 'EXLUSIVE_LOCK_STALL'
	__LOCK_EX_P = 'EXLUSIVE_LOCK_PENDING'
	__LOCK_EX_A = 'EXLUSIVE_LOCK_ACQUIRED'
	__LOCK_SH_P = 'SHARED_LOCK_PENDING'
	__LOCK_SH_A = 'SHARED_LOCK_ACQUIRED'
	
	__DEFAULT = 'DEFAULT'
	__PREPARE = 'PREPARE'
	__RETRY   = 'RETRY'
	__WAITING = 'WAITING'
	__EXEC    = 'EXECUTING'
	__DONE    = 'DONE'
	__FINISH  = 'FINISH'
	
	def __init__(self):
		self.__resources = {}
		self.__state = self.__DEFAULT
	
	def sh(self, resource):
		if self.__state is not self.__PREPARE:
			raise ValueError("You can use this method only inside the `prepare` method. Current state: " + self.__state)
		resource.on_locked_sh   = self.__on_locked
		resource.on_unlocked_sh = self.__on_unlocked
		resource.on_locked_ex   = self.__on_locked
		resource.on_unlocked_ex = self.__on_unlocked
		self.__resources[resource] = self.__LOCK_SH_P # lock pending
		r = resource.lock_sh()
		if not r:
			print("stall (sh)")
			self.__resources[resource] = self.__LOCK_EX_S # stall
	
	def ex(self, resource):
		if self.__state is not self.__PREPARE:
			raise ValueError("You can use this method only inside the `prepare` method. Current state: " + self.__state)
		resource.on_locked_sh   = self.__on_locked
		resource.on_unlocked_sh = self.__on_unlocked
		resource.on_locked_ex   = self.__on_locked
		resource.on_unlocked_ex = self.__on_unlocked
		self.__resources[resource] = self.__LOCK_EX_P # lock pending
		r = resource.lock_ex()
		if not r:
			print("stall (ex)")
			self.__resources[resource] = self.__LOCK_EX_S # stall
	
	def __on_locked(self, resource):
		print("on_locked")
		if resource not in self.__resources:
			if resource.locked_ex:
				self.__resources[resource] = self.__LOCK_EX_A
			elif resource.locked_sh:
				self.__resources[resource] = self.__LOCK_SH_A
		elif self.__resources[resource] is self.__LOCK_SH_P:
			if not resource.locked_sh:
				raise RuntimeError("Resource caled `on_locked_sh` despite `locked_sh` is false.")
			self.__resources[resource] = self.__LOCK_SH_A
		elif self.__resources[resource] is self.__LOCK_EX_P:
			if not resource.locked_ex:
				raise RuntimeError("Resource caled `on_locked_ex` despite `locked_ex` is false.")
			self.__resources[resource] = self.__LOCK_EX_A
		else:
			self.__resources[resource] = self.__LOCK_EX_S # stall
		
		if all((_v in (self.__LOCK_SH_A, self.__LOCK_EX_A)) for _v in self.__resources.values()): # all locks acquired
			if self.__state is self.__WAITING:
				self.__execute()
	
	def __on_unlocked(self, resource):
		print("on_unlocked")
		if resource not in self.__resources: return
		
		self.__resources[resource] = self.__UNLOCK
		
		if all((_v is self.__UNLOCK) for _v in self.__resources.values()): # all locks released
			if self.__state is self.__RETRY:
				self.__prepare()
			elif self.__state is self.__DONE:
				self.__finish()
	
	def serve(self):
		if self.__state is not self.__DEFAULT:
			raise ValueError("Method `serve` may be called only from DEFAULT state. Current state: " + self.__state)
		self.__prepare()
	
	def __prepare(self):
		try:
			self.__state = self.__PREPARE
			self.__resources = {}
			self.prepare()
			if any((_v is self.__LOCK_EX_S) for _v in self.__resources.values()):
				self.__retry()
			else:
				self.__state = self.__WAITING
				if all((_v in (self.__LOCK_SH_A, self.__LOCK_EX_A)) for _v in self.__resources.values()): # all locks acquired
					self.__execute()
		except:
			self.__state = self.__DONE
			self.__done()
			raise
	
	def __retry(self):
		self.__state = self.__RETRY
		for resource in self.__resources.keys():
			if self.__resources[resource] in (self.__LOCK_SH_A, self.__LOCK_SH_P):
				self.__resources[resource] = self.__UNLOCK_P
				resource.unlock_sh()
			elif self.__resources[resource] in (self.__LOCK_EX_A, self.__LOCK_EX_P):
				self.__resources[resource] = self.__UNLOCK_P
				resource.unlock_ex()
	
	def __execute(self):
		if self.__state is not self.__WAITING: return
		try:
			self.__state = self.__EXEC
			self.execute()
		finally:
			self.__done()
	
	def __done(self):
		self.__state = self.__DONE
		for resource in self.__resources.keys():
			if self.__resources[resource] is self.__LOCK_SH_A:
				self.__resources[resource] = self.__UNLOCK_P
				resource.unlock_sh()
			elif self.__resources[resource] in self.__LOCK_EX_A:
				self.__resources[resource] = self.__UNLOCK_P
				resource.unlock_ex()
			else:
				RuntimeError("Consistency error. A resource has impossible state at this point: " + self.__resources[resource])
	
	def __finish(self):
		try:
			self.__state = self.__FINISH
			self.finish()
		finally:
			self.__resources = {}
			self.__state = self.__DEFAULT
	
	def prepare(self):
		pass
	
	def execute(self):
		pass
	
	def finish(self):
		for resource in self.__resources.keys():
			try:
				resource.close()
			except:
				pass


