#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import dbus
import dbus.service

from . import ADDRESS_PAXOS
from ..interfaces.manager import Manager
from ..interfaces.paxos import Paxos


__all__ = ['paxos', 'setup', 'cleanup', 'Paxos', 'Manager', 'ADDRESS_PAXOS', 'INTERFACE_PAXOS', 'INTERFACE_MANAGER']


INTERFACE_MANAGER = Manager.INTERFACE
INTERFACE_PAXOS = Paxos.INTERFACE


def setup(bus, address=ADDRESS_PAXOS, path='/', debug=False):
	global paxos
	print("Starting service `paxos` (address='%s', path='%s')." % (address, path))
	paxos = Manager(Paxos, bus=bus, address=address, path=path)


def cleanup():
	global paxos
	print("Closing service `paxos`...")
	paxos.close()
	paxos.remove_from_connection()
	del paxos
	print("Service `paxos` closed.")



