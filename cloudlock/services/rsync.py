#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import nested_scopes, with_statement, generators, division, unicode_literals


import dbus
import dbus.service

from . import ADDRESS_RSYNC
from ..interfaces.manager import Manager
from ..interfaces.rsync import Rsync


__all__ = ['rsync', 'setup', 'cleanup', 'Rsync', 'Manager', 'ADDRESS_RSYNC', 'INTERFACE_RSYNC', 'INTERFACE_MANAGER']


INTERFACE_MANAGER = Manager.INTERFACE
INTERFACE_RSYNC = Rsync.INTERFACE


def setup(bus, address=ADDRESS_RSYNC, path='/'):
	global rsync
	print("Starting service `rsync` (address='%s', path='%s')." % (address, path))
	rsync = Manager(Rsync, bus=bus, address=address, path=path)


def cleanup():
	global rsync
	print("Closing service `rsync`...")
	rsync.close()
	rsync.remove_from_connection()
	del rsync
	print("Service `rsync` closed.")



