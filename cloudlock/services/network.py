#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import dbus
import dbus.service

from . import ADDRESS_NETWORK
from ..interfaces.network import Network, Domain, Peer


__all__ = ['network', 'setup', 'cleanup', 'Network', 'Domain', 'Peer', 'ADDRESS_NETWORK', 'INTERFACE_NETWORK', 'INTERFACE_DOMAIN', 'INTERFACE_PEER']


INTERFACE_NETWORK = Network.INTERFACE
INTERFACE_DOMAIN = Domain.INTERFACE
INTERFACE_PEER = Peer.INTERFACE


def setup(bus, address=ADDRESS_NETWORK, path='/', debug=False, cert_verify=True):
	global network
	print("Starting service `network` (address='%s', path='%s', debug=%s)." % (address, path, debug))
	network = Network(debug=debug, cert_verify=cert_verify, bus=bus, address=address, path=path)


def cleanup():
	global network
	print("Closing service `network`...")
	network.close()
	network.remove_from_connection()
	del network
	print("Service `network` closed.")



