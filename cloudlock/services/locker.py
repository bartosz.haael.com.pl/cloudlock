#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import dbus
import dbus.service

from . import ADDRESS_LOCKER
from ..interfaces.locker import Locker


__all__ = ['locker', 'setup', 'cleanup', 'Locker', 'ADDRESS_LOCKER', 'INTERFACE_LOCKER']


INTERFACE_LOCKER = Locker.INTERFACE


def setup(bus, address=ADDRESS_LOCKER, path='/'):
	global locker
	print("Starting service `locker` (address='%s', path='%s')." % (address, path))
	locker = Locker(bus=bus, address=address, path=path)


def cleanup():
	global locker
	print("Closing service `locker`...")
	locker.remove_from_connection()
	del locker
	print("Service `locker` closed.")



