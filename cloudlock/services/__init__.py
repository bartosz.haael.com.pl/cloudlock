#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


__all__ = ['ADDRESS_PAXOS', 'ADDRESS_LOCKER', 'ADDRESS_NETWORK', 'ADDRESS_RSYNC', 'run_service']


ADDRESS_PAXOS   = 'net.haael.paxos'
ADDRESS_LOCKER  = 'net.haael.locker'
ADDRESS_NETWORK = 'net.haael.network'
ADDRESS_RSYNC   = 'net.haael.rsync'


def run_service(setup, cleanup, **kwargs):
	print()
	
	from gi.repository import GObject
	from dbus import SessionBus
	from dbus.mainloop.glib import DBusGMainLoop
	from signal import signal, SIGTERM
	from threading import Thread
	from time import strftime
	
	class DummyProfiler:
		def start(self):
			pass
		
		def done(self):
			pass
	
	profiler = DummyProfiler()
	
	if 'profile' in kwargs:
		import pycallgraph
		import pycallgraph.output.graphviz
		profiler = pycallgraph.PyCallGraph(output=pycallgraph.output.graphviz.GraphvizOutput(output_file=kwargs['profile']))
		del kwargs['profile']
	
	print(strftime('%Y-%m-%d %H:%M:%S'))
	
	GObject.threads_init()
	
	mainloop = GObject.MainLoop()
	
	DBusGMainLoop(set_as_default=True)
	
	if ('bus' not in kwargs) or (kwargs['bus'] == 'SESSION'):
		bus = SessionBus()
	elif (kwargs['bus'] == 'SYSTEM'):
		bus = SystemBus()
	else:
		bus = SessionBus() # TODO: kwargs['bus']
	
	setup(bus, **kwargs)
	
	def ready_note():
		print(strftime('%Y-%m-%d %H:%M:%S'), "service ready")
		return False
	GObject.idle_add(ready_note)
	
	def main():
		profiler.start()
		try:
			mainloop.run()
		except KeyboardInterrupt:
			print()
		finally:
			profiler.done()
	mainthread = Thread(target=main)
	signal(SIGTERM, lambda number, stack: GObject.idle_add(lambda: mainloop.quit()))
	mainthread.start()
	mainthread.join()
	
	cleanup()
	
	bus.close()
	
	print(strftime('%Y-%m-%d %H:%M:%S'))
	print()


