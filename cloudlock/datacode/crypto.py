#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


from Crypto.Cipher import AES
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from Crypto.Util import asn1

from base64 import b64decode
from binascii import hexlify

from .random import random


__all__ = ['rsakey', 'encrypt', 'decrypt']



def rsakey(rawprivkey, password=None):
	# This is a hack to work around a bug in RSA.importKey that results with ValueError.
	# The function can not handle encrypted PEM strings that have length being a multiple
	# of 64 bytes.
	pemdata = rawprivkey.split(b"-----END RSA PRIVATE KEY-----")[0].split(b"-----BEGIN RSA PRIVATE KEY-----")[1]
	pemdata = b"".join([b"-----BEGIN RSA PRIVATE KEY-----", pemdata, b"========\n", b"-----END RSA PRIVATE KEY-----"])
	return RSA.importKey(pemdata, password)


def encrypt(text, key, signer=None):
	iv = random(AES.block_size)
	message = iv + AES.new(key, AES.MODE_CFB, iv).encrypt(text)
	if signer:
		signature = bytes(signer.sign(SHA256.new(message)))
	else:
		signature = bytes()
	l = len(signature)
	return bytes([l % 0x100, (l >> 8) % 0x100]) + signature + message


def decrypt(text, key, verifier=None):
	siglen = text[0] + text[1] << 8
	signature = text[2:siglen+2]
	message = text[siglen+2:]
	if verifier and not verifier.verify(signature, SHA256.new(message)):
		raise ValueError()
	iv = message[:AES.block_size]
	return AES.new(key, AES.MODE_CFB, iv).decrypt(message[AES.block_size:])


if __debug__:
	_key = "kobylamamalybok."
	for testcase in [b"abcd", b"kupa", b"aaaaaaaaaaaaaaaaaaaaaaaaaa", b"The celtic knights are calling me from behind."]:
		assert decrypt(encrypt(testcase, _key, None), _key, None) == testcase






