#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals

from Crypto import Random

__all__ = ['random']

_Random = Random.new()

def random(n):
	return _Random.read(n)






