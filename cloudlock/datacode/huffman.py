#!/usr/bin/python3
#-*- coding:utf-8 -*-


__all__ = ['encode', 'decode', 'init']




def bytes_wrap(f):
	def function(s):
		return bytes(f(s))
	function.__name__ = f.__name__
	return function



@bytes_wrap
def huffman_decode(s):
	global huffman_dec
	bits = []
	for l in s:
		il = int(l)
		n = len(bits)
		bits.extend([bool((il >> _i) & 1) for _i in range(8)])
		i = 1
		while i <= len(bits):
			try:
				yield huffman_dec[tuple(bits[:i])]
				del bits[:i]
				i = 1
			except KeyError:
				i += 1


@bytes_wrap
def huffman_encode(s):
	global huffman_enc
	bits = []
	for l in s:
		bits.extend(huffman_enc[l])
		if len(bits) >= 8:
			n = 0
			for b in reversed(bits[:8]):
				n <<= 1
				if b: n |= 1
			yield n
			del bits[:8]
	while bits:
		n = 0
		for b in reversed(bits[:8]):
			n <<= 1
			if b: n |= 1
		yield n
		del bits[:8]



def init_huffman_tables(freq, tl):
	global huffman_enc
	global huffman_dec
	
	corpus = []
	for k in freq.keys():
		corpus.append((k, freq[k] / tl))
	while len(corpus) > 1:
		corpus.sort(key=lambda x: x[1], reverse=True)
		a, ka = corpus.pop()
		b, kb = corpus.pop()
		corpus.append(((a, b), ka + kb))
	
	def gen(c):
		a, b = c
		if isinstance(a, tuple):
			for e in gen(a):
				yield (True,) + e[0], e[1]
		else:
			yield (True,), a
		if isinstance(b, tuple):
			for e in gen(b):
				yield (False,) + e[0], e[1]
		else:
			yield (False,), b
	
	huffman_enc = {}
	huffman_dec = {}
	for (x, y) in gen(corpus[0][0]):
		huffman_enc[y] = x
		huffman_dec[x] = y



#if __debug__:
#	init_huffman_tables()
#	for testcase in [b"abcd", b"kupa", b"aaaaaaaaaaaaaaaaaaaaaaaaaa", b"The celtic knights are calling me from behind."]:
#		assert decode(encode(testcase)) == testcase




