#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals

from Crypto.Hash import SHA256, MD5

__all__ = ['sha256sum', 'md5sum']


def sha256sum(s):
	return SHA256.new(s).digest()


def md5sum(s):
	return MD5.new(s).digest()


