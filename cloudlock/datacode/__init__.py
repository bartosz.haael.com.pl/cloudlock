#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


from .crypto import encrypt, decrypt
from .compress import compress, decompress


__all__ = ['encode', 'decode']


def encode(text, key, cert=None):
	return encrypt(compress(text), key, cert)


def decode(text, key, cert=None):
	return decompress(decrypt(text, key, cert))


