#!/usr/bin/python3
#-*- coding:utf-8 -*-


from .huffman import huffman_encode, huffman_decode, init_huffman_tables


__all__ = ['bwt_encode', 'bwt_decode', 'mtf_encode', 'mtf_decode', 'huffman_encode', 'huffman_decode', 'init_huffman_tables', 'compress', 'decompress']


def bwt_encode(s):
	"""Apply Burrows-Wheeler transform to the input string."""
	if bytes([2]) in s: raise ValueError("Input string cannot contain character \\2.")
	if bytes([3]) in s: raise ValueError("Input string cannot contain character \\3.")
	s = bytes([2]) + s + bytes([3])
	table = sorted(s[i:] + s[:i] for i in range(len(s)))
	last_column = [int(row[-1]) for row in table]
	return bytes(last_column)


'''
def bwt_decode(r): # FIXME: provide a better implementation
	"""Apply inverse Burrows-Wheeler transform."""
	table = [[]] * len(r)
	for i in range(len(r)):
		table = sorted([r[j]] + table[j] for j in range(len(r)))
	s = [row for row in table if (row[-1] == 3)][0]
	if s[0] != 2: raise ValueError("Decoded string does not start with character \\2.")
	if s[-1] != 3: raise ValueError("Decoded string does not end with character \\3.")
	return bytes(s[1:-1])
'''


def bwt_decode(r):
	firstCol = sorted(r)
	count = [0] * 256
	byteStart = [-1] * 256
	output = [0] * len(r)
	shortcut = [None] * len(r)
	#Generates shortcut lists
	for i in range(len(r)):
		shortcutIndex = r[i]
		shortcut[i] = count[shortcutIndex]
		count[shortcutIndex] += 1
		shortcutIndex = firstCol[i]
		if byteStart[shortcutIndex] == -1:
			byteStart[shortcutIndex] = i
		localIndex = r.index(3)
	for i in range(len(r)):
		#takes the next index indicated by the transformation vector
		nextByte = r[localIndex]
		output[len(r)-i-1] = nextByte
		shortcutIndex = nextByte
		#assigns localIndex to the next index in the transformation vector
		localIndex = byteStart[shortcutIndex] + shortcut[localIndex]
	
	if output[0] != 2: raise ValueError("Decoded string does not start with character \\2.")
	if output[-1] != 3: raise ValueError("Decoded string does not end with character \\3.")
	
	return bytes(output[1:-1])



def mtf_encode(s):
	"""Move-to-Front transform."""
	c = [_i for _i in range(256)]
	r = []
	for l in s:
		il = int(l)
		p = c.index(il)
		r.append(p)
		c.pop(p)
		c.insert(0, il)
	return bytes(r)


def mtf_decode(s):
	"""Inverse Move-to-Front transform."""
	c = [_i for _i in range(256)]
	r = []
	for l in s:
		il = int(l)
		p = c[il]
		r.append(p)
		c.pop(il)
		c.insert(0, p)
	return bytes(r)


def compress(s):
	t = mtf_encode(bwt_encode(s))
	return bytes([len(t) // 256, len(t) % 256]) + huffman_encode(t)


def decompress(s):
	return bwt_decode(mtf_decode(huffman_decode(s[2:])[:256*s[0]+s[1]]))



def generate_json_freq(json_list=None):
	if json_list is None:
		json1 = b'''
{
	   "firstName": "John",
    "lastName": "Smith",
    "age": 25,
    "address": {
        "streetAddress": "21 2nd Street",
        "city": "New York",
        "state": "NY",
        "postalCode": 10021
    },
    "phoneNumber": [
        {
            "type": "home",
            "number": "212 555-1234"
        },
        {
            "type": "fax",
            "number": "646 555-4567"
        }
    ]
}
'''
		
		json2 = b'''
{
    "name": "Product",
    "properties": {
        "id": {
            "type": "number",
            "description": "Product identifier",
            "required": true
        },
        "name": {
            "type": "string",
            "description": "Name of the product",
            "required": true
        },
        "price": {
            "type": "number",
            "minimum": 0,
            "required": true
        },
        "tags": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "stock": {
            "type": "object",
            "properties": {
                "warehouse": {
                    "type": "number"
                },
                "retail": {
                    "type": "number"
                }
            }
        }
    }
}
'''
		
		json3 = b'''
{
    "id": 1,
    "name": "Foo",
    "price": 123,
    "tags": [ "Bar", "Eek" ],
    "stock": {
        "warehouse": 300,
        "retail": 20
    }
}
'''
		
		json4 = b'''{"jsonrpc": "2.0", "method": "subtract", "params": [42, 23], "id": 1}'''
		
		json5 = b'''{
    "glossary": {
        "title": "example glossary",
		"GlossDiv": {
            "title": "S",
			"GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
                    },
					"GlossSee": "markup"
                }
            }
        }
    }
}
'''
		
		json6 = b'''{"menu": {
  "id": "file",
  "value": "File",
  "popup": {
    "menuitem": [
      {"value": "New", "onclick": "CreateNewDoc()"},
      {"value": "Open", "onclick": "OpenDoc()"},
      {"value": "Close", "onclick": "CloseDoc()"}
    ]
  }
}}'''
		
		json7 = b'''{"widget": {
    "debug": "on",
    "window": {
        "title": "Sample Konfabulator Widget",
        "name": "main_window",
        "width": 500,
        "height": 500
    },
    "image": { 
        "src": "Images/Sun.png",
        "name": "sun1",
        "hOffset": 250,
        "vOffset": 250,
        "alignment": "center"
    },
    "text": {
        "data": "Click Here",
        "size": 36,
        "style": "bold",
        "name": "text1",
        "hOffset": 250,
        "vOffset": 100,
        "alignment": "center",
        "onMouseUp": "sun1.opacity = (sun1.opacity / 100) * 90;"
    }
}}    '''
		
		json_list = [json1, json2, json3, json4, json5, json6, json7]
	
	freq = {}
	for i in range(256):
		freq[i] = 0
	tl = 0
	
	for s0 in json_list:
		s1 = bwt_encode(s0)
		s2 = mtf_encode(s1)
		for l in s2:
			freq[l] += 1
		tl += len(s2)
	
	return freq, tl
	

init_huffman_tables(*generate_json_freq())


if __debug__:
	for testcase in [b"abcd", b"kupa", b"aaaaaaaaaaaaaaaaaaaaaaaaaa", b"The celtic knights are calling me from behind."]:
		assert decompress(compress(testcase)) == testcase




