#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


#from ..util.random import random
from Crypto import Random as CryptoRandom


_Random = CryptoRandom.new()

def random(n):
	return _Random.read(n)



__all__ = []


def store_data(store, position, data):
	length = len(data)
	for start in sorted(store.keys()):
		stop = start + len(store[start])
		if position < start and start <= position + length < stop:
			data = data + store[start][-stop+position+length:]
			del store[start]
			store[position] = data
			break
		elif start <= position < stop and start <= position + length < stop:
			olddata = store[start]
			newdata = olddata[:position-start] + data + olddata[-stop+position+length:]
			store[start] = newdata
			break
		elif start < position <= stop and stop <= position + length:
			data = store[start][:position-start] + data
			length += position-start
			position = start
		elif position <= start and stop <= position + length:
			del store[start]
	else:
		store[position] = data


def encrypt(key, position, data_in):
	l = len(key)
	data_out = []
	for n, b in enumerate(data_in):
		k = key[(n + position) % l]
		data_out.append((b + k) % 256)
	return bytes(data_out)


def decrypt(key, position, data_in):
	l = len(key)
	data_out = []
	for n, b in enumerate(data_in):
		k = key[(n + position) % l]
		data_out.append((b - k) % 256)
	return bytes(data_out)


class Node:
	def __init__(self):
		self.__key = random(512)
		self.__data = {}
	
	def store(self, name, position, data):
		if name not in self.__data:
			self.__data[name] = {}
		store_data(self.__data[name], position, data)
	
	def fetch(self, name):
		try:
			return self.__data[name]
		except KeyError:
			return {}
	
	def encrypt_stored(self, key):
		for datastore in self.__data.values():
			for position, data in datastore.items():
				datastore[position] = encrypt(key, position, data)
	
	def decrypt_stored(self, key):
		for datastore in self.__data.values():
			for position, data in datastore.items():
				datastore[position] = decrypt(key, position, data)
	
	def encryption_key(self):
		return self.__key
	
	def decryption_key(self):
		return self.__key



class Store:
	def __init__(self, nodes=[]):
		self.__nodes = nodes[:]
	
	def add_node(self, node):
		for o_node in self.__nodes:
			o_node.encrypt_stored(node.encryption_key())
		self.__nodes.append(node)
	
	def remove_node(self, node):
		self.__nodes.remove(node)
		for o_node in self.__nodes:
			o_node.decrypt_stored(node.decryption_key())
	
	def store(self, name, position, data):
		l = len(data)
		d = len(self.__nodes)
		s = l // d
		for n, node in enumerate(self.__nodes):
			p0 = n * s
			p1 = ((n + 1) * s) if (n != d - 1) else None
			c_data = data[p0:p1]
			for c_node in self.__nodes:
				if c_node is node: continue
				c_data = encrypt(c_node.encryption_key(), position + p0, c_data)
			node.store(name, position + p0, c_data)
	
	def fetch(self, name):
		r = {}
		for node in self.__nodes:
			for position, data in node.fetch(name).items():
				for c_node in self.__nodes:
					if c_node is node: continue
					data = decrypt(c_node.decryption_key(), position, data)
				store_data(r, position, data)
		return r


#cleartext = b"abcdefghijklmnopqrstuvwxyz"



#a.store('a/tmp', 0, b"abcd")
#for pos, data in a.fetch('a/tmp').items():
#	b.store('a', pos, data)

#b.store('a/tmp', 4, b"efgh")
#for pos, data in b.fetch('a/tmp').items():
#	a.store('a', pos, data)



#a.encrypt(b.public_key())
#b.encrypt(a.public_key())

#nodes = [a, b]



a = Node()
b = Node()
c = Node()

s = Store([a, b])
s.store('a', 0, b"abcdefgh")

print(a._Node__data)
print(b._Node__data)
print(c._Node__data)
print(s.fetch('a'))
print()

s.add_node(c)

print(a._Node__data)
print(b._Node__data)
print(c._Node__data)
print(s.fetch('a'))
print()

s.store('b', 0, b"0123456789")

print(a._Node__data)
print(b._Node__data)
print(c._Node__data)
print(s.fetch('b'))
print()



