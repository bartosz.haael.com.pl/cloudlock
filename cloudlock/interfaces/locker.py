#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import dbus
import dbus.service
from collections import defaultdict

from . import INTERFACE_LOCKER


__all__ = ['Locker', 'INTERFACE_LOCKER']


class Locker(dbus.service.Object):
	INTERFACE = INTERFACE_LOCKER
	
	def __init__(self, bus, address, path):
		dbus.service.Object.__init__(self, dbus.service.BusName(address, bus=bus), path)
		self.__lock_sh = defaultdict(lambda: [])
		self.__lock_ex = defaultdict(lambda: [])
	
	@dbus.service.method(dbus_interface=INTERFACE_LOCKER, in_signature='s', out_signature='b', sender_keyword='sender')
	def lock_sh(self, path, sender='?'):
		print("lock_sh('%s', sender='%s')" % (path, sender))
		if path in self.__lock_ex:
			return False
		else:
			self.__lock_sh[path].append(sender)
			return True
	
	@dbus.service.method(dbus_interface=INTERFACE_LOCKER, in_signature='s', out_signature='b', sender_keyword='sender')
	def lock_ex(self, path, sender='?'):
		print("lock_ex('%s', sender='%s')" % (path, sender))
		if path in self.__lock_ex:
			return False
		else:
			if not self.__lock_sh[path]:
				self.__lock_ex[path] = (sender, True)
				self.locked(path)
			else:
				self.__lock_ex[path] = (sender, False)
			return True
	
	@dbus.service.method(dbus_interface=INTERFACE_LOCKER, in_signature='s', out_signature='b', sender_keyword='sender')
	def unlock_sh(self, path, sender='?'):
		print("unlock_sh('%s', sender='%s')" % (path, sender))
		if (path not in self.__lock_sh) or (sender not in self.__lock_sh[path]):
			return False
		else:
			self.__lock_sh[path].remove(sender)
			if (not self.__lock_sh[path]) and (path in self.__lock_ex):
				self.__lock_ex[path] = (self.__lock_ex[path][0], True)
				self.locked(path)
			if not self.__lock_sh[path]:
				del self.__lock_sh[path]
			return True
	
	@dbus.service.method(dbus_interface=INTERFACE_LOCKER, in_signature='s', out_signature='b', sender_keyword='sender')
	def unlock_ex(self, path, sender='?'):
		print("unlock_ex('%s', sender='%s')" % (path, sender))
		if (path not in self.__lock_ex) or (self.__lock_ex[path][0] != sender):
			return False
		else:
			if self.__lock_ex[path][1]:
				self.unlocked(path)
			del self.__lock_ex[path]
			return True
	
	def update_external_lock(self, path, owner):
		if owner == self.__name:
			pass
		elif owner:
			fcntl.flock(path, fcntl.LOCK_EX)
		else:
			fcntl.flock(path, fcntl.LOCK_NONE)
	
	def acquire_external_lock(self, path):
		extlock = self.__paxos.fetch(path)
		if extlock is not None:
			owner = extlock['owner']
			if owner == self.__peer_name:
				#announce locked
				return True
			date = extlock['date']
			if (date - time() < 30) and (date - time()) > -self.__MAX_OFFSET:
				#announce will not lock
				return False
		self.__paxos.send(path, {'owner':self.__name, 'date':time()})
	
	def release_external_lock(self, path):
		extlock = self.__paxos.fetch(path)
		if extlock is not None:
			owner = extlock['owner']
			if owner == self.__peer_name:
				self.__paxos.send(path, None)
	
	@dbus.service.method(dbus_interface=INTERFACE_LOCKER, in_signature='s', out_signature='(bb)')
	def is_locked(self, path):
		return ((path in self.__lock_sh) and (self.__lock_sh[path])), (path in self.__lock_ex)
	
	@dbus.service.method(dbus_interface=INTERFACE_LOCKER, in_signature='', out_signature='a{s(assb)}')
	def locks_list(self):
		raise NotImplementedError()
		keys = self.__lock_sh.keys() + self.__lock_ex.keys()
		ret = []
		for key in keys:
			if key in self.__lock_sh:
				i = self.__lock_sh[key]
			else:
				i = []
			
			if key in self.__lock_ex:
				b = self.__lock_ex[key]
			else:
				b = (None, False)
			
			ret.append((key, i) + b)
		return ret
	
	@dbus.service.signal(dbus_interface=INTERFACE_LOCKER, signature='s')
	def locked(self, path):
		pass
	
	@dbus.service.signal(dbus_interface=INTERFACE_LOCKER, signature='s')
	def unlocked(self, path):
		pass


