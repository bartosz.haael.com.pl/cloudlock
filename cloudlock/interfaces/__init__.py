#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import dbus


__all__ = ['DBusClientObject', 'ConnectPaxosWithDomain', 'INTERFACE_MANAGER', 'INTERFACE_PAXOS', 'INTERFACE_DIRECTORY', 'INTERFACE_LOCKER', 'INTERFACE_NETWORK', 'INTERFACE_DOMAIN', 'INTERFACE_PEER', 'INTERFACE_RSYNC']


INTERFACE_MANAGER   = 'net.haael.Manager'
INTERFACE_PAXOS     = 'net.haael.Paxos'
INTERFACE_DIRECTORY = 'net.haael.Directory'
INTERFACE_LOCKER    = 'net.haael.Locker'
INTERFACE_NETWORK   = 'net.haael.Network'
INTERFACE_DOMAIN    = 'net.haael.Domain'
INTERFACE_PEER      = 'net.haael.Peer'
INTERFACE_RSYNC     = 'net.haael.Rsync'


class DBusClientObject:
	def __init__(self, bus, address, path, interface=None):
		self._bus = bus
		self._address = address
		self.__dbus_object_path__ = path
		self.__interface = interface
		if self.__interface is not None:
			self.__object = dbus.Interface(bus.get_object(address, path), interface)
		else:
			self.__object = bus.get_object(address, path)
	
	def _interface(self, interface):
		if self.__interface is not None:
			raise ValueError("This object already has an interface.")
		if interface is None:
			raise ValueError("Supplied argument must be a string.")
		self.__interface = interface
		self.__object = dbus.Interface(self.__object, interface)
		return self
	
	def connect_to_signal(self, *args, **kwargs):
		return self.__object.connect_to_signal(*args, **kwargs)
	
	def __getattr__(self, attr):
		orig_method = self.__object.get_dbus_method(attr)
		if orig_method:
			def new_method(*args, **kwargs):
				result = orig_method(*args, **kwargs)
				if isinstance(result, dbus.ObjectPath):
					result = DBusClientObject(self._bus, self._address, result)
				return result
			return new_method
		else:
			return getattr(self.__object, attr)


class ConnectPaxosWithDomain:
	def __init__(self, paxos, domain, resource, callback=None):
		self.__paxos = paxos
		self.__domain = domain
		self.__resource = resource
		self.__callback = callback
		
		peer_list = domain.list_peers()
		self.__paxos.set_peer_name(self.__domain.host_name())
		self.__paxos.set_peer_list(peer_list)
		self.__peer = dict([(str(_peer), self.__domain.get_peer(_peer)._interface(INTERFACE_PEER)) for _peer in peer_list])
		
		self.__paxos.connect_to_signal('message_out', self.message_out)
		self.__paxos.connect_to_signal('failure', self.failure)
		
		for name, peer in self.__peer.items():
			recv_message = (lambda peer_name: lambda resource, address, payload: self.recv_message(peer_name, resource, address, payload))(name)
			peer.connect_to_signal('recv_message', recv_message)
		
		self.__domain.connect_to_signal('peer_added', self.peer_added)
		self.__domain.connect_to_signal('peer_deleted', self.peer_deleted)
	
	def peer_added(self, name, peer):
		self.__peer[name] = DBusClientObject(self.__domain._bus, self.__domain._address, peer, INTERFACE_PEER)
		recv_message = (lambda peer_name: lambda resource, address, payload: self.recv_message(peer_name, resource, address, payload))(name)
		self.__peer[name].connect_to_signal('recv_message', recv_message)
		self.__paxos.set_peer_list(self.__peer.keys())
		if self.__callback:
			self.__callback(self.__paxos, self.__domain, self.__resource, 'peer_added', (name,))
	
	def peer_deleted(self, name, peer):
		del self.__peer[name]
		self.__paxos.set_peer_list(self.__peer.keys())
		if self.__callback:
			self.__callback(self.__paxos, self.__domain, self.__resource, 'peer_deleted', (name,))
	
	def message_out(self, peer, payload):
		self.__peer[peer].send_message(self.__resource, payload)
		if self.__callback:
			self.__callback(self.__paxos, self.__domain, self.__resource, 'message_out', (payload,))
	
	def failure(self, key, value, version):
		#self.__paxos.send(key, value) # try again
		if self.__callback:
			self.__callback(self.__paxos, self.__domain, self.__resource, 'failure', (key, value))
	
	def recv_message(self, peer, resource, address, payload):
		if resource != self.__resource:
			return
		self.__paxos.message_in(peer, payload)
		if self.__callback:
			self.__callback(self.__paxos, self.__domain, self.__resource, 'recv_message', (address, payload))
	
	def close(self):
		pass # TODO


