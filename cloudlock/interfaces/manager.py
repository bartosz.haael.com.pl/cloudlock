#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import dbus
import dbus.service

from . import INTERFACE_MANAGER


__all__ = ['Manager', 'INTERFACE_MANAGER']


class Manager(dbus.service.Object):
	INTERFACE = INTERFACE_MANAGER
	
	def __init__(self, constructor, bus, address, path='/'):
		dbus.service.Object.__init__(self, dbus.service.BusName(address, bus=bus), path)
		self.__constructor = constructor
		self.__bus = bus
		self.__address = address
		self.__objs = {}
		#print("manager created:", self.INTERFACE, constructor, constructor.INTERFACE, bus, address, path)
	
	@dbus.service.method(dbus_interface=INTERFACE_MANAGER, in_signature='sav', variant_level=1, out_signature='o')
	def acquire(self, tag, params):
		try:
			return self.__objs[tag]
		except KeyError:
			path = tag.replace('.', '/')
			while path[0] == '/':
				path = path[1:]
			path = '/' + path
			#print("constructor:", self.__constructor, type(self.__constructor))
			self.__objs[tag] = self.__constructor(*params, bus=self.__bus, address=self.__address, path=path)
			return self.__objs[tag]
	
	@dbus.service.method(dbus_interface=INTERFACE_MANAGER, in_signature='s', out_signature='b')
	def release(self, tag):
		try:
			self.__objs[tag].remove_from_connection()
			del self.__objs[tag]
			return True
		except KeyError:
			return False
	
	def close(self):
		for obj in self.__objs.values():
			try:
				obj.close()
			except AttributeError:
				pass
			obj.remove_from_connection()


