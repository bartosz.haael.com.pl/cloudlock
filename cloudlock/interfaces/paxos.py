#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import dbus
import dbus.service

from time import time
from socket import *
from socket import error as socket_error
import json

from . import INTERFACE_PAXOS, INTERFACE_DIRECTORY


__all__ = ['Paxos', 'Directory', 'INTERFACE_PAXOS', 'INTERFACE_DIRECTORY']



class Paxos(dbus.service.Object):
	INTERFACE = INTERFACE_PAXOS
	
	__MAX_VOTING_TIME = 10.0
	__MIN_SYNC_TIME   = 0.25
	
	def __init__(self, bus, address, path):
		dbus.service.Object.__init__(self, dbus.service.BusName(address, bus=bus), path)
		self.__name = None
		self.__value = {}
		self.__proposal = {}
		self.__peers = []
		self.__bus = bus
	
	@dbus.service.method(dbus_interface=INTERFACE_PAXOS, in_signature='sos', out_signature='')
	def connect_paxos_with_domain(self, domain_address, domain_path, resource): #FIXME
		from . import INTERFACE_DOMAIN, DBusClientObject, ConnectPaxosWithDomain
		domain = DBusClientObject(self.__bus, domain_address, domain_path, INTERFACE_DOMAIN)
		self.__connection = ConnectPaxosWithDomain(self, domain, resource)
	
	@dbus.service.method(dbus_interface=INTERFACE_PAXOS, in_signature='sa{sv}', out_signature='', variant_level=1)
	def send(self, key, value):
		# TODO: add voting timeout

		if self.__name is None:
			raise ValueError("Set the peer name first.")
		
		print("send", self.__name, time())
		print(" key =", key)
		print(" value =", value)
		print(" peers list =", "(" + str(len(self.__peers)) + ")", self.__peers)
		print()
		
		version = time()
		
		try:
			pvalue, pversion = self.__value[key]
			if pversion >= version - self.__MIN_SYNC_TIME:
				self.failure(key, value, version)
				return
		except KeyError:
			pass
		
		try:
			pvalue, pversion, porigin, pvotes = self.__proposal[key]
			if pvalue == value:
				return # update will be called later
			elif pversion >= version - self.__MAX_VOTING_TIME:
				self.failure(key, value, version)
				return
			else:
				del self.__proposal[key]
		except KeyError:
			pass
		
		if len(self.__peers) == 0:
			self.update(key, value, version)
		else:
			self.__proposal[key] = [value, version, self.__name, set([self.__name])]
			for peer in self.__peers:
				self.message_out(peer, { 'key':key, 'value':value, 'version':version, 'origin':self.__name, 'votes':[self.__name] })
	
	@dbus.service.method(dbus_interface=INTERFACE_PAXOS, in_signature='s', out_signature='a{sv}', variant_level=1)
	def fetch(self, key):
		if self.__name is None:
			raise ValueError("Set the peer name first.")
		
		try:
			return self.__value[key][0]
		except KeyError:
			return {}
	
	@dbus.service.signal(dbus_interface=INTERFACE_PAXOS, signature='sa{sv}d')
	def update(self, key, value, version):
		self.__value[key] = [value, version]
		try:
			del self.__proposal[key]
		except KeyError:
			pass
		print("update", self.__name, time())
		print(" key =", key)
		print(" value =", value)
		print(" version =", version)
		print()
	
	@dbus.service.signal(dbus_interface=INTERFACE_PAXOS, signature='sa{sv}d')
	def failure(self, key, value, version):
		del self.__proposal[key]
		print("failure", self.__name, time())
		print(" key =", key)
		print(" value =", value)
		print(" version =", version)
		print()
	
	@dbus.service.signal(dbus_interface=INTERFACE_PAXOS, signature='isss')
	def error(self, number, message, peer, details):
		print("error", self.__name, number, time())
		print(" message =", message)
		print(" peer =", peer)
		print(" details =", details)
		print()
	
	@dbus.service.signal(dbus_interface=INTERFACE_PAXOS, signature='sa{sv}')
	def message_out(self, peer, payload):
		print("message_out", self.__name, time())
		print(" destination peer =", peer)
		print(" payload =", payload)
		print()
	
	@dbus.service.method(dbus_interface=INTERFACE_PAXOS, in_signature='sa{sv}', out_signature='', variant_level=1)
	def message_in(self, peer, payload):
		peer = str(peer)
		
		print("message_in", self.__name, time())
		print(" source peer =", peer)
		print(" payload =", dict([_x for _x in payload.items()]))
		
		if peer not in self.__peers:
			self.error(1, "Message from a peer not in the peer list.", peer, "")
			print(" (unknown peer error)")
			print()
			return
		
		try:
			key = payload['key']
			value = payload['value']
			version = payload['version']
			origin = payload['origin']
			votes = set([str(_s) for _s in payload['votes']])
			print(" votes =", votes)
			votes.intersection_update(self.__peers + [self.__name])
			print(" peers =", self.__peers)
			print(" votes & peers =", votes)
		except KeyError as e:
			self.error(2, "Malformed message.", peer, str(e))
			print(" (malformed message error)")
			print()
			return
		
		if key in self.__proposal:
			pvalue, pversion, porigin, pvotes = self.__proposal[key]
			
			print(" assert", self.__name, "in", pvotes)
			assert self.__name in pvotes
			
			if origin == self.__name: # our proposal
				print(" ((A) vote for our proposal)")
				pvotes.update(votes)
				if len(pvotes) > (len(self.__peers) + 1) // 2: # majority
					print(" (majority reached)")
					print()
					for npeer in self.__peers:
						self.message_out(npeer, { 'key':key, 'value':value, 'version':version, 'origin':origin, 'votes':list(pvotes) })
					self.update(key, value, version)
				else:
					print()
				return
			elif porigin == self.__name: # other conflicting proposal
				print(" ((B) vote for other proposal)")
				if len(votes) > (len(self.__peers) + 1) // 2: # we are outnumbered
					print(" (majority reached)")
					print()
					self.failure(key, pvalue, pversion) # failure of our proposal
					self.update(key, value, version)
				else:
					print(" (remove votes)")
					pvotes.difference_update(votes) # remove nay votes
					print()
				return
			elif (value == pvalue) and (version == pversion): # third party proposal - normal voting
				print(" ((C) third party proposal)")
				pvotes.update(votes)
				if len(pvotes) > (len(self.__peers) + 1) // 2: # majority
					print(" (majority reached)")
					print()
					self.update(key, value, version)
				else:
					print()
				return
			# TODO: change vote when outnumbered but no majority
			# TODO: change vote based on priority
			#elif ((pversion < version) and (pversion > version - self.__MAX_VOTING_TIME))
			elif len(votes) > (len(self.__peers) + 1) // 2: # another voting reached majority
				print(" ((D) higher priority proposal - change vote)")
				print()
				# drop the current proposal
				del self.__proposal[key]
				votes.update([self.__name]) # give yes vote
				self.message_out(porigin, { 'key':key, 'value':value, 'version':version, 'origin':origin, 'votes':list(votes) }) # inform origin of failure
				# continue to voting
				print("... message_in")
			else:
				print(" ((E) lower priority proposal - keep vote)")
				print()
				# keep the current proposal
				self.message_out(origin, { 'key':key, 'value':pvalue, 'version':pversion, 'origin':porigin, 'votes':list(pvotes) }) # inform origin of failure
				return
		else:
			try:
				pvalue, pversion = self.__value[key]
				if (value == pvalue) and (version == pversion): # everything up to date
					print(" (value up to date already)")
					print()
					return
				if (pversion < version) and (pversion > version - self.__MIN_SYNC_TIME): # another voting started too early
					print(" (voting started too early)")
					print()
					return
			except KeyError:
				pass
		
		assert key not in self.__proposal
		
		votes.update([self.__name]) # give yes vote
		if len(votes) > (len(self.__peers) + 1) // 2: # majority
			print(" (majority reached)")
			print()
			if origin == self.__name:
				for npeer in self.__peers:
					self.message_out(npeer, { 'key':key, 'value':value, 'version':version, 'origin':origin, 'votes':list(votes) })
			else:
				self.message_out(origin, { 'key':key, 'value':value, 'version':version, 'origin':origin, 'votes':list(votes) })
			self.update(key, value, version)
		else:
			print(" (proposal recorded)")
			print()
			self.__proposal[key] = [value, version, origin, votes]
			self.message_out(origin, { 'key':key, 'value':value, 'version':version, 'origin':origin, 'votes':list(votes) })
	
	@dbus.service.method(dbus_interface=INTERFACE_PAXOS, in_signature='s', out_signature='')
	def set_peer_name(self, name):
		self.__name = str(name)
	
	@dbus.service.method(dbus_interface=INTERFACE_PAXOS, in_signature='as', out_signature='')
	def set_peer_list(self, peers):
		if self.__name is None:
			raise ValueError("Set the peer name first.")
		if self.__name in peers:
			raise ValueError("Peer list must not contain this peer name.")
		
		self.__peers = [str(_peer) for _peer in peers]
	
	@dbus.service.method(dbus_interface=INTERFACE_PAXOS, in_signature='s', out_signature='o')
	def get_directory(self, path):
		directory = Directory(self.__bus, self.__address, path, self)
		self.__directories.append(directory)
		return directory
	
	def close_directory(self, directory):
		self.__directories.remove(directory)
		directory.remove_from_connection()
	
	def close(self):
		for directory in self.__directories[:]:
			self.close_directory(directory)
		
		try:
			self.__connection.close()
		except AttributeError:
			pass


class Directory(dbus.service.Object):
	def __init__(self, bus, address, path, paxos):
		if path[-1] == '/':
			raise ValueError()
		pass
	
	@dbus.service.method(dbus_interface=INTERFACE_DIRECTORY, in_signature='sa{sv}', out_signature='')
	def send(self, key, value):
		if (not key) or (key[0] != "/"):
			key = "/" + key
		self.__paxos.send(self.__path + key, value)
	
	@dbus.service.method(dbus_interface=INTERFACE_DIRECTORY, in_signature='s', out_signature='a{sv}')
	def fetch(self, key):
		if (not key) or (key[0] != "/"):
			key = "/" + key
		return self.__paxos.fetch(self.__path + key)
	
	@dbus.service.signal(dbus_interface=INTERFACE_DIRECTORY, signature='sa{sv}ds')
	def update(self, key, value, version, origin):
		pass
	
	@dbus.service.signal(dbus_interface=INTERFACE_DIRECTORY, signature='sa{sv}d')
	def failure(self, key, value, version):
		pass
	
	@dbus.service.method(dbus_interface=INTERFACE_DIRECTORY, in_signature='', out_signature='')
	def close(self):
		self.__paxos.close_directory(self)




