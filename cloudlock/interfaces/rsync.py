#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import os
import time

import dbus
import dbus.service

from . import DBusClientObject, INTERFACE_RSYNC, INTERFACE_PAXOS


__all__ = ['Rsync', 'INTERFACE_RSYNC']



class Rsync(dbus.service.Object):
	INTERFACE = INTERFACE_RSYNC
	
	def __init__(self, root, host, paxos_address, paxos_path, bus, address, path):
		dbus.service.Object.__init__(self, dbus.service.BusName(address, bus=bus), path)
		self.__paxos = DBusClientObject(bus, paxos_address, paxos_path, INTERFACE_PAXOS)
		if root[-1] != "/":
			raise ValueError("The first argument must be an existing absolute directory path, ending with '/'.")
		self.__root = root
		self.__host = host
	
	def properties(self, local):
		"""Return properties of a local file."""
		
		if not local:
			return {}
		
		path = self.__root + local
		
		try:
			prop = os.stat(path)
		except OSError:
			return {}
		
		mtime = prop.st_mtime_ns
		size = prop.st_size
		
		data = open(path, 'rb')
		if size > 256:
			hashalg = hashlib.new('sha256')
			hashalg.update(data)
			filehash = hashalg.digest()
		else:
			filehash = data.read()
		data.close()
		
		properties = {'date':str(mtime), 'hash':hex(filehash)[2:], 'size':size}
		return properties
	
	def download(self, local, remote, host, version, filehash):
		self.synchronized(local, remote)
	
	@dbus.service.method(dbus_interface=INTERFACE_RSYNC, in_signature='ss', out_signature='b')
	def update(self, local, remote):
		"""
		Indicate the file 'local' as the newest version of the resource 'remote'.
		If 'local' is an empty string, this means a request to remove the resource.
		On success return True. On failure return False.
		"""
		properties = self.properties(local)
		if properties:
			properties['host'] = self.__host
		return self.__paxos.send(remote, properties)
	
	@dbus.service.method(dbus_interface=INTERFACE_RSYNC, in_signature='s', out_signature='d')
	def version(self, remote):
		"""Return the date of the last modification of the resource 'remote'."""
		try:
			return float(self.__paxos.fetch(remote)['date'])
		except KeyError:
			return 0
	
	@dbus.service.method(dbus_interface=INTERFACE_RSYNC, in_signature='ss', out_signature='b')
	def synchronize(self, local, remote):
		"""
		Copy the newest version of the resource 'remote' to the file 'local'.
		If the file is already synchronized, the function returns 'True'.
		If the file is not synchronized, it returns 'False' and schedules the synchronization.
		After the file has been updated, the 'synchronized' signal is invoked.
		"""
		
		remote_properties = self.__paxos.fetch(remote)
		if not remote_properties:
			self.delete(local)
			return True
		
		host = remote_properties['host']
		
		if host == self.__host:
			local_properties = self.properties(local)
			del remote_properties['host']
			if remote_properties == local_properties:
				return True
		
		self.download(local, remote, host, remote_properties['version'], remote_properties['hash'])
		return False
	
	@dbus.service.method(dbus_interface=INTERFACE_RSYNC, in_signature='ss', out_signature='b')
	def is_synchronized(self, local, remote):
		remote_properties = self.__paxos.fetch(remote)
		if not remote_properties:
			return False
		
		host = remote_properties['host']
		
		if host == self.__host:
			local_properties = self.properties(local)
			del remote_properties['host']
			if remote_properties == local_properties:
				return True
		
		return False
	
	@dbus.service.signal(dbus_interface=INTERFACE_RSYNC, signature='ss')
	def synchronized(self, local, remote):
		pass



