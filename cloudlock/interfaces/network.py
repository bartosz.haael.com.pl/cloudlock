#!/usr/bin/python3
#-*- coding:utf-8 -*-

from __future__ import unicode_literals


import socket
import ssl
import json
import time
import binascii
import os
import fcntl
import termios
import array

import dbus
import dbus.service

from gi.repository import GObject as gobject

from . import INTERFACE_NETWORK, INTERFACE_DOMAIN, INTERFACE_PEER
from ..datacode import encode as data_encode, decode as data_decode
from ..datacode.crypto import rsakey
from ..datacode.hash import sha256sum
from ..datacode.random import random


__all__ = ['Network', 'Domain', 'Peer', 'INTERFACE_NETWORK', 'INTERFACE_DOMAIN', 'INTERFACE_PEER']


class Network(dbus.service.Object):
	INTERFACE = INTERFACE_NETWORK
	
	__MAGIC = "cloudlock-0"
	__SSL_CERT_AUTH_PATH = "/etc/ssl/certs"
	__MAX_OFFSET = 0.1 # maximum difference between peer clocks (float, in seconds)
	__MAX_DELAY = 8.0 # maximum delay for packet reception (float, in seconds)
	
	def __init__(self, debug, cert_verify, bus, address, path):
		dbus.service.Object.__init__(self, dbus.service.BusName(address, bus=bus), path)
		self.bus = bus #FIXME
		self.address = address #FIXME
		#self.path = path #FIXME
		self.debug = debug
		self.cert_verify = cert_verify
		assert not (cert_verify and not debug)
		self.domains = {}
		self.tcp = {}
		self.udp = {}
		self.peers = {}
		self.outgoing = {}
	
	@dbus.service.method(dbus_interface=INTERFACE_NETWORK, in_signature='ssss', out_signature='o')
	def create_domain(self, hostname, domainname, certfile, keypass):
		if self.__dbus_object_path__[-1] == '/': #FIXME: locations
			path = self.__dbus_object_path__ + domainname.replace('.', '/')
		else:
			path = self.__dbus_object_path__ + '/' + domainname.replace('.', '/')
		domain = Domain(domainname, hostname, self, False, certfile, None, keypass, self.__SSL_CERT_AUTH_PATH, None, self.bus, self.address, path)
		self.domains[domainname] = domain
		return domain
	
	@dbus.service.method(dbus_interface=INTERFACE_NETWORK, in_signature='ssbsssss', out_signature='o')
	def create_domain_debug(self, hostname, domainname, debug, certfile, keyfile, keypass, capath, cafile):
		debug = bool(debug)
		if (debug) and (not self.debug):
			raise ValueError("Debug mode not enabled.")
		if self.__dbus_object_path__[-1] == '/': #FIXME: locations
			path = self.__dbus_object_path__ + domainname.replace('.', '/')
		else:
			path = self.__dbus_object_path__ + '/' + domainname.replace('.', '/')
		domain = Domain(domainname, hostname, self, debug, certfile, keyfile, keypass, capath, cafile, self.bus, self.address, path)
		self.domains[domainname] = domain
		return domain
	
	@dbus.service.method(dbus_interface=INTERFACE_NETWORK, in_signature='s', out_signature='o')
	def get_domain(self, domainname):
		return self.domains[domainname]
	
	@dbus.service.method(dbus_interface=INTERFACE_NETWORK, in_signature='s', out_signature='')
	def delete_domain(self, domainname):
		self.domains[domainname].close()
		del self.domains[domainname]
	
	@dbus.service.method(dbus_interface=INTERFACE_NETWORK, in_signature='', out_signature='as')
	def list_domains(self):
		return list(self.domains.keys())
	
	@dbus.service.method(dbus_interface=INTERFACE_NETWORK, in_signature='', out_signature='')
	def close(self):
		for domain in list(self.domains.values()):
			domain.close()
	
	@dbus.service.signal(dbus_interface=INTERFACE_NETWORK, signature='isvs')
	def rejection(self, code, message, address, details):
		print("rejection", code, time.time())
		print(" message =", message)
		print(" address =", address)
		print(" details =", repr(details))
		print()
	
	def tcp_connect(self, remote_address, local_address, domain, resource):
		#print("tcp_connect", remote_address, local_address, domain, resource)
		peer_raw = socket.socket(self.family(remote_address), socket.SOCK_STREAM)
		peer_raw.setblocking(False)
		if local_address:
			peer_raw.bind(local_address)
		
		def do_connect(_peer_raw, cond):
			try:
				peer_raw.connect(remote_address)
			except socket.error as error:
				if error.errno == 115:
					return True
				else:
					peer_raw.close()
					raise
			else:
				peer_raw.send((domain.domainname + '\n').encode('utf-8'))
				self.__credentials(peer_raw, remote_address, domain, resource)
			return False
		
		try:
			peer_raw.connect(remote_address)
		except socket.error as error:
			if error.errno == 115:
				gobject.io_add_watch(peer_raw, gobject.IO_OUT, do_connect)
			else:
				peer_raw.close()
				raise
		else:
			peer_raw.send((domain.domainname + '\n').encode('utf-8'))
			self.__credentials(peer_raw, remote_address, domain, resource)
	
	def tcp_accept(self, server, cond):
		if cond & gobject.IO_IN:
			client_raw, address = server.accept()
			client_raw.setblocking(False)
			self.__credentials(client_raw, address, None, None)
		
		if cond & gobject.IO_HUP:
			return False
		else:
			return True
	
	def __credentials(self, peer_raw, address, domain, resource):
		data = bytes()
		
		def receive(_peer_raw, cond):
			nonlocal data
			
			if cond & gobject.IO_HUP:
				peer_raw.close()
				self.rejection(1, "Connection closed unexpectedly.", address, "")
				return False
			
			if cond & gobject.IO_IN:
				data += peer_raw.recv(2048)
				if (b"\n" in data):
					name = data.split(b"\n")[0].decode('utf-8')
					if (domain) and (domain.domainname == name):
						self.__starttls(peer_raw, address, domain, resource, False)
					elif (not domain) and (name in self.domains.keys()):
						peer_raw.send((name + "\n").encode('utf-8')) # send back the domain name
						self.__starttls(peer_raw, address, self.domains[name], resource, True)
					else:
						peer_raw.close()
						self.rejection(2, "Invalid domain name or garbage data.", address, data)
					return False
				elif len(data) > 2048:
					peer_raw.close()
					self.rejection(3, "Domain name too long or garbage data.", address, data)
					return False
				else:
					return True
			
			raise RuntimeError("Unexpected condition.")
		
		gobject.io_add_watch(peer_raw, gobject.IO_IN | gobject.IO_HUP, receive)
	
	def __starttls(self, peer_raw, address, domain, resource, server_side):
		peer_tls = domain.ssl_context.wrap_socket(peer_raw, server_side=server_side, do_handshake_on_connect=False)
		
		def handshake(_peer_tls, cond):
			try:
				peer_tls.do_handshake()
			except ssl.SSLError as error:
				if error.args[0] == ssl.SSL_ERROR_WANT_READ:
					return True
				else:
					self.disconnect(peer_tls)
					self.rejection(4, "SSL error.", address, repr(error))
			else:
				self.__authenticate(peer_tls, address, domain, resource)
			return False
		
		try:
			peer_tls.do_handshake()
		except ssl.SSLError as error:
			if error.args[0] == ssl.SSL_ERROR_WANT_READ:
				gobject.io_add_watch(peer_tls, gobject.IO_IN | gobject.IO_HUP, handshake)
			else:
				self.disconnect(peer_tls)
				self.rejection(4, "SSL error.", address, str(error))
		else:
			self.__authenticate(peer_tls, address, domain, resource)
	
	def __authenticate(self, peer_tls, address, domain, resource):
		gobject.io_add_watch(peer_tls, gobject.IO_IN | gobject.IO_HUP, self.__verify, address, domain, resource)
		data = {}
		data['magic'] = self.__MAGIC
		data['hostname'] = domain.hostname
		data['domainname'] = domain.domainname
		data['tcp'] = list(_k for (_k, (_, _, _d)) in self.tcp.items() if domain.domainname in _d)
		data['udp'] = list(_k for (_k, (_, _, _d)) in self.udp.items() if domain.domainname in _d)
		data['outgoing'] = list(_k for (_k, _d) in self.outgoing.items() if domain.domainname in _d)
		if resource:
			data['resource'] = resource
		data['password'] = binascii.b2a_hex(domain.password).decode('utf-8')
		data['time'] = time.time()
		peer_tls.write((json.dumps(data) + "\n").encode('utf-8'))
	
	def __verify(self, client_tls, cond, address, domain, local_resource):
		if not (cond & gobject.IO_IN):
			self.disconnect(client_tls)
			self.rejection(5, "No data.", address, "")
			return False
		
		r = client_tls.read() # TODO: buffering
		
		try:
			k = json.loads(r.decode('utf-8'))
			del r
		except (ValueError, UnicodeDecodeError):
			self.disconnect(client_tls)
			self.rejection(6, "Received data is not a valid JSON.", address, r)
			return False
		
		try:
			magic = k['magic']
			hostname = k['hostname']
			domainname = k['domainname']
			tcpaddr = set(tuple(_a) for _a in k['tcp'])
			udpaddr = set(tuple(_a) for _a in k['udp'])
			outgoing = set(k['outgoing'])
			password = binascii.a2b_hex(k['password'].encode('utf-8'))
			time_ = k['time']
		except KeyError as keyerror:
			self.disconnect(client_tls)
			self.rejection(7, "Received JSON object does not have all the required fields.", address, str(keyerror))
			return False
		except ValueError as valueerror:
			self.disconnect(client_tls)
			self.rejection(8, "Received JSON object fields malformed.", address, str(keyerror))
			return False
		
		try:
			remote_resource = k['resource']
		except KeyError:
			remote_resource = None
		
		del k
		
		if magic != self.__MAGIC:
			self.disconnect(client_tls)
			self.rejection(9, "Magic value mismatch.", address, magic)
			return False
		
		curtime = time.time()
		if time_ > curtime + self.__MAX_OFFSET or time_ < curtime - self.__MAX_DELAY - self.__MAX_OFFSET:
			self.disconnect(client_tls)
			self.rejection(10, "Timestamp outside allowed range.", address, str(curtime))
			return False
		
		if not outgoing:
			self.disconnect(client_tls)
			self.rejection(11, "Outgoing address list empty.", address, "")
			return False
		
		try:
			if self.family(address) in [socket.AF_INET, socket.AF_INET6]:
				baseaddr = address[0]
			else:
				baseaddr = str(address)
		except ValueError:
			self.disconnect(client_tls)
			self.rejection(12, "Unknown address family.", address, baseaddr)
			return False
		
		if baseaddr not in outgoing:
			self.disconnect(client_tls)
			self.rejection(13, "Address not in allowed list.", address, baseaddr)
			return False
		
		if domainname != domain.domainname:
			self.disconnect(client_tls)
			self.rejection(14, "Domain mismatch.", address, domainname)
			return False
		
		if hostname == domain.hostname:
			self.disconnect(client_tls)
			self.rejection(15, "Hostname is the same as ours.", address, hostname)
			return False
		
		if not tcpaddr:
			self.disconnect(client_tls)
			self.rejection(16, "TCP address list empty.", address, "")
			return False
		for addr in tcpaddr:
			try:
				f = self.family(addr[0])
				if not self.debug:
					if (f == socket.AF_UNIX):
						self.disconnect(client_tls)
						self.rejection(17, "Unix domain sockets allowed only in debug mode.", address, str(addr))
						return False
			except ValueError:
				self.disconnect(client_tls)
				self.rejection(18, "Received TCP address malformed.", address, str(addr))
				return False
		
		if not udpaddr:
			self.disconnect(client_tls)
			self.rejection(19, "UDP address list empty.", address, "")
			return False
		for addr in udpaddr:
			try:
				f = self.family(addr[0])
				if not self.debug:
					if (f == socket.AF_UNIX):
						self.disconnect(client_tls)
						self.rejection(20, "Unix domain sockets allowed only in debug mode.", address, str(addr))
						return False
				#TODO: IP address ranges
			except ValueError:
				self.disconnect(client_tls)
				self.rejection(21, "Received UDP address malformed.", address, str(addr))
				return False
		
		if self.cert_verify:
			try:
				cert = client_tls.getpeercert(False) # textual certificate
				ssl.match_hostname(cert, hostname + "." + domainname)
			except ssl.CertificateError as e:
				self.disconnect(client_tls)
				self.rejection(22, "Could not get certificate.", address, str(e))
				return False
		
		certificate = client_tls.getpeercert(True) # binary certificate
		
		# verified
		if hostname in domain.peers:
			peer = domain.peers[hostname]
			
			if self.cert_verify and peer.certificate != certificate:
				self.disconnect(client_tls)
				self.rejection(23, "Wrong certificate for this peer.", address, binascii.b2a_hex(sha256sum(certificate)) + b" vs " + binascii.b2a_hex(sha256sum(peer.certificate)))
			elif peer.hostname != hostname:
				self.disconnect(client_tls)
				self.rejection(24, "Existing peer hostname mismatch.", address, hostname)
			elif peer.domain.domainname != domainname:
				self.disconnect(client_tls)
				self.rejection(25, "Existing peer domain mismatch.", address, domainname)
			elif (not local_resource) and (not remote_resource): # configuration request
				self.disconnect(client_tls)
				domain.peer_configuration(address, hostname, tcpaddr, udpaddr, outgoing, password)
			elif baseaddr not in peer.outgoing:
				self.disconnect(client_tls)
				self.rejection(26, "Address not in allowed list.", address, str(baseaddr))
			elif local_resource and remote_resource:
				self.disconnect(client_tls)
				self.rejection(27, "Invalid resource identifier.", address, remote_resource)
			elif cond & gobject.IO_HUP: # connection closed, just a configuration request
				self.disconnect(client_tls)
			else:
				if remote_resource:
					peer.tcp_redirect_incoming(remote_resource, address, client_tls)
				elif local_resource:
					peer.tcp_redirect_outgoing(local_resource, address, client_tls)
				else:
					raise RuntimeError("Either `local_resource` or `remote_resource` must be set.")
		else:
			self.disconnect(client_tls)
			domain.peer_invitation(address, hostname, list(tcpaddr), list(udpaddr), list(outgoing), password, certificate)
		return False
	
	def disconnect(self, peer_tls):
		try:
			peer_tls.shutdown(socket.SHUT_RDWR)
		except socket.error:
			pass
		peer_tls.close()
	
	@staticmethod
	def family(addr):
		if isinstance(addr, str):
			return socket.AF_UNIX
		
		if isinstance(addr, tuple) and len(addr) == 4:
			return socket.AF_INET6
		
		if isinstance(addr, tuple) and len(addr) == 2:
			if ':' in addr[0]:
				return socket.AF_INET6
			else:
				return socket.AF_INET
		
		raise ValueError("Could not determine address family: " + repr(addr))
	
	def udp_recv(self, udp_socket, cond):
		if cond & gobject.IO_IN:
			indata, address = udp_socket.recvfrom(2048)
			
			if self.family(address) in [socket.AF_INET, socket.AF_INET6]:
				baseaddr = address[0]
			else:
				baseaddr = address
			
			try:
				domainname, rawdata = indata.split(b"\n", 1)
				domainname = domainname.decode('utf-8')
			except (ValueError, UnicodeDecodeError):
				self.rejection(28, "Garbage data on UDP socket.", address, indata)
				return False
			
			del indata
			
			try:
				domain = self.domains[domainname]
			except KeyError:
				self.rejection(29, "No such domain", address, domainname)
				return False
			
			for peer in domain.peers.values():
				if baseaddr not in peer.outgoing:
					continue
				
				try:
					jsondata = data_decode(rawdata, peer.password) #TODO: signing
				except ValueError:
					continue
				except IndexError:
					continue
				
				try:
					record = json.loads(jsondata.decode('utf-8'))
				except ValueError:
					continue
				
				try:
					magic = record['magic']
					hostname = record['hostname']
					domainname = record['domainname']
					resource = record['resource']
					payload = record['payload']
					time_ = record['time']
					#print(type(time_), time_)
				except KeyError as k:
					self.rejection(30, "JSON object received from UDP does not have all required fields.", address, str(k))
					break
				
				if magic != self.__MAGIC:
					self.rejection(31, "Magic value mismatch.", address, magic)
					break
				
				curtime = time.time()
				if time_ > curtime + self.__MAX_OFFSET or time_ < curtime - self.__MAX_DELAY - self.__MAX_OFFSET:
					self.rejection(32, "Timestamp outside allowed range.", address, " ".join([str(time_), str(curtime), str(time_ - curtime)]))
					break
				
				if domainname != domain.domainname:
					self.rejection(33, "Domain mismatch.", address, domainname)
					break
				
				# everything OK
				
				peer.recv_message(resource, address, payload)
				break
			else:
				self.rejection(34, "Packet not from a registered peer.", address, "")
			return True
		return False
	
	def udp_send(self, remote_address, local_address, domain, resource, payload):
		data = {}
		data.update({ 'magic':self.__MAGIC, 'hostname':domain.hostname, 'domainname':domain.domainname })
		data.update({ 'resource':resource, 'payload':payload, 'salt':binascii.b2a_hex(random(2)).decode('utf-8') })
		data['time'] = time.time()
		jsondata = json.dumps(data).encode('utf-8')
		
		if local_address:
			udp_socket = self.udp[local_address][0]
		else:
			udp_socket = list(self.udp.values())[0][0]
		
		message = data_encode(jsondata, domain.password) #TODO: signing
		message = domain.domainname.encode('utf-8') + b"\n" + message
		#print(remote_address, message)
		udp_socket.sendto(message, remote_address)


class Domain(dbus.service.Object):
	INTERFACE = INTERFACE_DOMAIN
	
	def __init__(self, domainname, hostname, network, debug, certfile, keyfile, password, capath, cafile, bus, address, path):
		print("create_domain", hostname + "." + domainname, time.strftime("%Y-%m-%d %H:%M:%S"))
		print(" debug =", debug)
		print(" certfile =", repr(certfile))
		print(" keyfile =", repr(keyfile))
		print(" keyfile_password_present =", (password is not None))
		print(" capath =", repr(capath))
		print(" cafile =", repr(cafile))
		
		dbus.service.Object.__init__(self, dbus.service.BusName(address, bus=bus), path)
		self.bus = bus #FIXME
		self.address = address #FIXME
		#self.path = path #FIXME
		self.network = network
		self.domainname = domainname
		self.hostname = hostname
		self.debug = debug
		self.password = random(16)
		self.peers = {}
		
		self.ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
		#self.ssl_context.options |= ssl.OP_SINGLE_DH_USE | ssl.OP_SINGLE_ECDH_USE
		if not self.debug:
			self.ssl_context.verify_mode = ssl.CERT_REQUIRED
		else:
			self.ssl_context.verify_mode = ssl.CERT_OPTIONAL
		self.ssl_context.set_ciphers('HIGH:!aNULL:!eNULL')
		
		if not capath: capath = None
		if not cafile: cafile = None
		self.ssl_context.load_verify_locations(capath=capath, cafile=cafile)
		
		if not certfile: certfile = None
		if not keyfile: keyfile = None
		try:
			try:
				self.ssl_context.load_cert_chain(certfile=certfile, keyfile=keyfile, password=lambda: password)
			except TypeError: # Python pre 3.3 doesn`t have the 'password' argument.
				if password: # Hack: push the password back to the terminal input buffer, byte by byte. FIXME: The password is visible on the terminal.
					tty = 0 #os.open("/dev/tty", os.O_RDWR | os.O_NOCTTY)
					for char in bytes(password, encoding='utf-8'):
						fcntl.ioctl(tty, termios.TIOCSTI, array.array('b', [char]))
					fcntl.ioctl(tty, termios.TIOCSTI, b"\n")
					self.ssl_context.load_cert_chain(certfile=certfile, keyfile=keyfile)
					#os.close(tty)
					#raise TypeError("This version of Python doesn`t support ssl keys with passwords.")
				else:
					self.ssl_context.load_cert_chain(certfile=certfile, keyfile=keyfile)
		except IOError: # bad password
			raise ValueError("Bad password for private key.")
		
		with open(keyfile or certfile, 'rb') as c:
			self.ssl_signer = rsakey(c.read(), password)
		
		print() # final log line, after all possible error messages
		
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='v', variant_level=1, out_signature='')
	def tcp_open(self, address):
		try:
			self.network.tcp[address][2].add(self.domainname)
		except KeyError:
			f = self.network.family(address)
			if not self.debug and (f == socket.AF_UNIX):
				raise ValueError("Unix domain sockets allowed only in debug mode.")
			sock = socket.socket(f, socket.SOCK_STREAM)
			sock.bind(address)
			sock.listen(4)
			sock_ev = gobject.io_add_watch(sock, gobject.IO_IN | gobject.IO_HUP, self.network.tcp_accept)
			self.network.tcp[address] = [sock, sock_ev, set([self.domainname])]
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='v', variant_level=1, out_signature='')
	def udp_open(self, address):
		try:
			self.network.udp[address][2].add(self.domainname)
		except KeyError:
			f = self.network.family(address)
			if not self.debug and (f == socket.AF_UNIX):
				raise ValueError("Unix domain sockets allowed only in debug mode.")
			sock = socket.socket(f, socket.SOCK_DGRAM)
			sock.bind(address)
			sock.setblocking(False)
			sock_ev = gobject.io_add_watch(sock, gobject.IO_IN | gobject.IO_HUP, self.network.udp_recv)
			self.network.udp[address] = [sock, sock_ev, set([self.domainname])]
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='v', variant_level=1, out_signature='')
	def tcp_close(self, address):
		if (self.domainname is not None) and (self.domainname not in self.tcp[address][2]):
			return
		if self.domainname is not None:
			self.network.tcp[address][2].remove(self.domainname)
		if not self.network.tcp[address][2]:
			gobject.source_remove(self.network.tcp[address][1])
			self.network.tcp[address][0].close()
			del self.network.tcp[address]
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='v', variant_level=1, out_signature='')
	def udp_close(self, address):
		if (self.domainname is not None) and (self.domainname not in self.udp[address][2]):
			return
		if self.domainname is not None:
			self.network.udp[address][2].remove(self.domainname)
		if not self.network.tcp[address][2]:
			gobject.source_remove(self.network.udp[address][1])
			self.udp[address][0].close()
			del self.network.udp[address]
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='s', out_signature='')
	def add_outgoing(self, address):
		try:
			self.network.outgoing[address].append(self.domainname)
		except KeyError:
			self.network.outgoing[address] = [self.domainname]
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='s', out_signature='')
	def del_outgoing(self, address):
		self.network.outgoing[address].remove(self.domainname)
		if not self.network.outgoing[address]:
			del self.network.outgoing[address]
	
	@dbus.service.signal(dbus_interface=INTERFACE_DOMAIN, signature='vsvvvayay')
	def peer_invitation(self, address, hostname, tcpaddr, udpaddr, outgoing, password, certificate):
		print("peer_invitation", self.hostname + "." + self.domainname, time.strftime("%Y-%m-%d %H:%M:%S"))
		print(" address =", address)
		print(" name =", hostname + "." + self.domainname)
		print(" TCP address list =", tcpaddr)
		print(" UDP address list =", udpaddr)
		print(" outgoing address list =", outgoing)
		print(" password =", binascii.b2a_hex(password))
		print(" certificate fingerprint =", binascii.b2a_hex(sha256sum(certificate)))
		print()
	
	@dbus.service.signal(dbus_interface=INTERFACE_DOMAIN, signature='vasavavavay')
	def peer_configuration(self, address, hostname, tcpaddr, udpaddr, outgoing, password):
		print("peer_configuration", self.hostname + "." + self.domainname, time.strftime("%Y-%m-%d %H:%M:%S"))
		print(" address =", address)
		print(" name =", hostname + "." + self.domainname)
		print(" TCP address list =", tcpaddr)
		print(" UDP address list =", udpaddr)
		print(" outgoing address list =", outgoing)
		print(" password =", binascii.b2a_hex(password))
		print()
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='say', variant_level=1, out_signature='o')
	def add_peer(self, hostname, certificate):
		try:
			self.peers[hostname].close() # close any existing peer
			self.peer_deleted(hostname, self.peers[hostname])
		except KeyError:
			pass
		if self.__dbus_object_path__[-1] == '/': #FIXME: locations
			path = self.__dbus_object_path__ + hostname.replace('.', '/')
		else:
			path = self.__dbus_object_path__ + '/' + hostname.replace('.', '/')
		peer = Peer(hostname, self, certificate, self.bus, self.address, path)
		self.peers[hostname] = peer
		self.peer_added(hostname, peer)
		return peer
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='s', out_signature='')
	def del_peer(self, hostname):
		try:
			self.peers[hostname].close()
			self.peer_deleted(hostname, self.peers[hostname])
			del self.peers[hostname]
			return True
		except KeyError:
			return False
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='s', out_signature='o')
	def get_peer(self, hostname):
		return self.peers[hostname]
	
	@dbus.service.signal(dbus_interface=INTERFACE_DOMAIN, signature='so')
	def peer_added(self, name, peer):
		pass
	
	@dbus.service.signal(dbus_interface=INTERFACE_DOMAIN, signature='so')
	def peer_deleted(self, name, peer):
		pass
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='', out_signature='as')
	def list_peers(self):
		return list(self.peers.keys())
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='', out_signature='s')
	def domain_name(self):
		return self.domainname
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='', out_signature='s')
	def host_name(self):
		return self.hostname
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='', out_signature='s')
	def fqdn_name(self):
		return ".".jois([self.hostname, self.domainname])
	
	@dbus.service.method(dbus_interface=INTERFACE_DOMAIN, in_signature='v', variant_level=1, out_signature='')
	def invite(self, address):
		self.network.tcp_connect(address, None, self, "")
	
	def close(self):
		for peer in self.peers.values():
			peer.close()
		self.peers = {}
		#self.remove_from_connection()


class Peer(dbus.service.Object):
	INTERFACE = INTERFACE_PEER
	CONNECTION_TIMEOUT = 8.0
	
	def __init__(self, hostname, domain, certificate, bus, address, path):
		assert hostname is not None
		assert domain is not None and hasattr(domain, 'domainname')
		assert certificate is not None
		dbus.service.Object.__init__(self, dbus.service.BusName(address, bus=bus), path)
		self.hostname = hostname
		self.domain = domain
		self.certificate = bytes(certificate)
		self.tcp = set()
		self.udp = set()
		self.outgoing = set()
		self.password = bytes()
		self.__pending = {}
		print("peer created", self.hostname + "." + self.domain.domainname)
		print(" certificate figerprint =", binascii.b2a_hex(sha256sum(self.certificate)))
		print()
	
	@dbus.service.method(dbus_interface=INTERFACE_PEER, in_signature='avavavay', variant_level=1, out_signature='')
	def configure(self, tcpaddr, udpaddr, outgoing, password):
		self.tcp = set(tcpaddr)
		self.udp = set(udpaddr)
		self.outgoing = set(outgoing)
		self.password = bytes(password)
		self.__outbound = []
		self.__inbound = []
		print("peer configuration", self.hostname + "." + self.domain.domainname)
		print(" certificate figerprint =", binascii.b2a_hex(sha256sum(self.certificate)))
		print(" tcp =", [_x for _x in self.tcp])
		print(" udp =", [_x for _x in self.tcp])
		print(" outgoing =", [_x for _x in self.tcp])
		print(" password hash =", binascii.b2a_hex(sha256sum(self.password)))
		print()
	
	def __bind_to_fd(self, stream, endpoint):
		#print("registering endpoint:", endpoint)
		fcntl.fcntl(endpoint, fcntl.F_SETFL, fcntl.fcntl(endpoint, fcntl.F_GETFL) | os.O_NONBLOCK)
		endpoint = os.fdopen(endpoint, 'wb+', buffering=0)
		#endpoint = socket.fromfd(endpoint)
		
		
		def net_to_fd(_s, cond):
			if cond & gobject.IO_IN:
				try:
					endpoint.write(stream.recv())
				except ssl.SSLError as error:
					if error.errno == 8:
						self.domain.network.disconnect(stream)
						endpoint.close()
						return False
					raise
				except IOError as error:
					if error.errno == 32:
						self.domain.network.disconnect(stream)
						endpoint.close()
						return False
					raise
				except ValueError:
					return False
			
			if cond & gobject.IO_HUP:
				self.domain.network.disconnect(stream)
				endpoint.close()
				return False
			
			return True
		
		def fd_to_net(_e, cond):
			if cond & gobject.IO_IN:
				try:
					stream.send(endpoint.read())
				except ssl.SSLError as error:
					if error.errno == 8:
						self.domain.network.disconnect(stream)
						endpoint.close()
						return False
					raise
				except IOError as error:
					if error.errno == 32:
						self.domain.network.disconnect(stream)
						endpoint.close()
						return False
					raise
				except ValueError:
					return False
			
			if cond & gobject.IO_HUP:
				self.domain.network.disconnect(stream)
				endpoint.close()
				return False
			
			return True
		
		gobject.io_add_watch(stream, gobject.IO_IN | gobject.IO_HUP, net_to_fd)
		gobject.io_add_watch(endpoint, gobject.IO_IN | gobject.IO_HUP, fd_to_net)
	
	def tcp_redirect_incoming(self, resource, address, stream):
		todel = []
		curtime = time.time()
		
		for n, (o_resource, o_stream, timeout) in enumerate(self.__inbound):
			if timeout + self.CONNECTION_TIMEOUT < curtime:
				self.domain.network.disconnect(stream)
				todel.append(n)
		
		for n in reversed(todel):
			del self.__inbound[n]
		
		self.__inbound.append((resource, stream, time.time()))
		self.incoming_connection(resource)
	
	def tcp_redirect_outgoing(self, resource, address, stream):
		todel = []
		curtime = time.time()
		found = False
		
		for n, (o_resource, endpoint, timeout) in enumerate(self.__outbound):
			if timeout + self.CONNECTION_TIMEOUT < curtime:
				endpoint.close()
				self.connection_error(0, "Timeout", o_resource)
				todel.append(n)
			elif not found and resource == o_resource:
				self.__bind_to_fd(stream, endpoint)
				todel.append(n)
				found = True
		
		for n in reversed(todel):
			del self.__outbound[n]
		
		if not found:
			# log warning
			print("Spurious outgoing connection: ", ("domain=" + self.domain.domainname), ("peer=" + self.hostname), ("resource=" + resource), ("address=" + str(address)))
	
	@dbus.service.method(dbus_interface=INTERFACE_PEER, in_signature='sh', out_signature='')
	def create_connection(self, resource, endpoint):
		endpoint = endpoint.take()
		self.__outbound.append((resource, endpoint, time.time()))
		remote_address = list(self.tcp)[0] #TODO: address selection
		local_address = None
		self.domain.network.tcp_connect(remote_address, local_address, self.domain, resource)
	
	@dbus.service.method(dbus_interface=INTERFACE_PEER, in_signature='sh', out_signature='')
	def accept_connection(self, resource, endpoint):
		endpoint = endpoint.take()
		todel = []
		curtime = time.time()
		found = False
		
		for n, (o_resource, stream, timeout) in enumerate(self.__inbound):
			if timeout + self.CONNECTION_TIMEOUT < curtime:
				self.domain.network.disconnect(stream)
				todel.append(n)
			elif not found and resource == o_resource:
				self.__bind_to_fd(stream, endpoint)
				todel.append(n)
				found = True
		
		for n in reversed(todel):
			del self.__inbound[n]
		
		if not found:
			self.connection_error(1, "Resource not found", resource)
	
	@dbus.service.signal(dbus_interface=INTERFACE_PEER, signature='s')
	def incoming_connection(self, resource):
		pass
	
	@dbus.service.method(dbus_interface=INTERFACE_PEER, in_signature='sa{sv}', variant_level=1, out_signature='')
	def send_message(self, resource, payload):
		#print("send_message", resource, payload)
		remote_address = list(self.udp)[0] #TODO: address selection
		local_address = None
		rpayload = {}
		for k, v in payload.items():
			#print((k, v))
			rpayload[str(k)] = v
		self.domain.network.udp_send(remote_address, local_address, self.domain, resource, rpayload)
	
	@dbus.service.signal(dbus_interface=INTERFACE_PEER, signature='sva{sv}')
	def recv_message(self, resource, address, payload):
		print("recv_message", self.domain.hostname + "." + self.domain.domainname, time.strftime("%Y-%m-%d %H:%M:%S"))
		print(" host", "from='" + self.hostname + "'", "to='" + self.domain.hostname + "'")
		print(" resource = '" + resource + "'")
		print(" address =", address)
		print(" payload =", payload)
		print()
	
	@dbus.service.signal(dbus_interface=INTERFACE_PEER, signature='iss')
	def connection_error(self, number, message, resource):
		pass
	
	def close(self):
		# TODO: cleanup
		self.remove_from_connection()




